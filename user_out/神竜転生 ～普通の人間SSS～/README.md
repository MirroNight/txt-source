# novel

- title: 神竜転生 ～普通の人間になるため転生したら全パラメーターSSSでした～
- author: 年中麦茶太郎@ZZT231
- source: http://ncode.syosetu.com/n5963et/
- cover:
- publisher: syosetu
- date: 2018-05-31T12:00:00+08:00
- status: 連載

## series

- name: 神竜転生 ～普通の人間になるため転生したら全パラメーターSSSでした～

## preface


```
世界を救った神竜アマデウスは「人間になりたい」と神様にお願いした。
そして異世界に転生し、人間の少女になる。
自分は普通の人間だと思い込むアマデウスだが、本来Ｓランクまでしかない世界で彼女だけＳＳＳランク。
あらゆる攻撃に耐性を持ち、仮に傷を負っても細胞が一つでも残っていたら瞬時に再生。
完全に規格外の力を持つ彼女に、異世界の神様や天使は大慌て。
だが今まで銀河を破壊するような敵と戦ってきたアマデウスからすれば、星の一つも壊せないＳＳＳの何が凄いのか分からない。
「私は普通ですよ。個性の範疇です」
本人はそう言うが、人間だけでなく神や魔族を含めた中でもブッチギリで世界最強だった。
```

## tags

- node-novel
- syosetu
- コメディー
- ファンタジー
- 主人公最強
- 剣と魔法
- 女主人公
- 微百合

# contribute

- 休闲飛
- 

# options

## syosetu

- txtdownload_id: 1235840

## textlayout

- allow_lf2: true

# link

- [dip.jp](https://narou.dip.jp/search.php?text=n5963et&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [神龙转生为人_sss参数很普通呢](https://tieba.baidu.com/f?kw=%E7%A5%9E%E9%BE%99%E8%BD%AC%E7%94%9F%E4%B8%BA%E4%BA%BA_sss%E5%8F%82%E6%95%B0%E5%BE%88%E6%99%AE%E9%80%9A%E5%91%A2&ie=utf-8 "神龙转生为人_sss参数很普通呢")
- 


