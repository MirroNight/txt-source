# English

[TOC]

## 00000_一章　魔王降臨/00030_002 大森林　★

### 'sodom'

- '文為「Sodom」，源'

### 'mild heaven'

- '──　Mild Heaven\n大帝'

### 'chai night'

- 'Chai Night等品牌'


## 00000_一章　魔王降臨/00040_003 魔王與悪　★

### 'heterochromia iridis'

- '色症（Heterochromia Iridis），反'


## 00000_一章　魔王降臨/00050_004 願望祠堂

### 'satan ring'

- '之戒（Satan Ring）\n\n'


## 00010_二章　黄金の聖女/00090_006 聖光国

### 'elegant'

- '崗特=Elegant，既高'


## 00010_二章　黄金の聖女/00100_007 悄然接近之影

### 'own goal'

- '\n\n「Own Goal？」\n'
- '（颯：Own Goal是指足'


## 00010_二章　黄金の聖女/00110_008 聖女　★

### 'gold slash'

- '刃》（Gold Slash）！」'


## 00010_二章　黄金の聖女/00160_014 悪魔信奉者　★

### 'satanist'

- '團──Satanist。\n\n'

### 'utopia'

- '邦」（Utopia）的人'

### 'walking'

- '金格（Walking）發出'

### 'ice hammer'

- '冰錘/Ice Hammer》」\n'

### 'gold splash'

- '槍衾/Gold Splash》！」'


## 00010_二章　黄金の聖女/00170_015 龍人　★

### '750rs'

- '──　750RS（ＺⅡ'


## 00020_三章　神都動乱/00210_018 魔王軍開始行動

### 'mild heaven'

- '夫叼著Mild Heaven，點上'

### 'congratulations'

- '\n\n\nCONGRATULATIONS──！'


## 00020_三章　神都動乱/00260_023 毫无慈悲的侵略

### 'piun'

- '\n什麼Piun⋯⋯未'
- '了⋯⋯Piun」\n\n'
- '密⋯⋯Piun！」\n'
- '望⋯⋯Piun」\n\n'
- '的⋯⋯Piun」\n\n'
- '邊⋯⋯Piun」\n\n'

### 'wusa'

- '明白了Wusa」\n\n'
- '蘿蔔了Wusa！」\n'
- '說什麼Wusa」\n\n'

### 'bar'

- '如今在BAR或者拉'
- '（颯：BAR指酒店'


## 00020_三章　神都動乱/00270_024 神聖之国

### 'marshal'

- '夏魯（Marshal）。安'


## 00020_三章　神都動乱/00300_027 魔王的冰山一角

### 'ice slash'

- '冰刃/Ice Slash！》」'

### 'ice splash'

- '槍衾/Ice Splash》」\n'


## 00020_三章　神都動乱/00310_028 悪魔VS魔王

### 'summon devil'

- '召喚（Summon Devil）──'

### 'carnival'

- '魯」（Carnival）召喚'
- '（颯：Carnival是歐美'


## 00020_三章　神都動乱/00320_029 童話游戯

### 'house'

- '房子（House）吧⋯'


## 00020_三章　神都動乱/00330_030 前往聖城

### 'holy rain'

- '之雨/Holy Rain》」\n'

### 'bubble cure'

- '之泡/Bubble Cure》」\n'


## 00020_三章　神都動乱/00340_031 正義漢子

### 'angel cross'

- '聖衣/Angel Cross》」\n'


## 00030_四章　魔王の躍動/035 魔人達

### 'uma'

- '\n\n「UMA嗎？？'
- '就像是UMA一樣啊'


## 00030_四章　魔王の躍動/038 温泉旅館

### 'piun'

- '了⋯⋯Piun！」\n'
- '怖⋯⋯Piun」\n\n'
- '情⋯⋯Piun！」\n'
- '軟⋯⋯Piun！」\n'

### 'wusa'

- '以享受Wusa」\n\n'
- '是暴行Wusa」\n\n'
- '瘋了～Wusa」\n\n'
- '義不明Wusa」\n\n'
- '點⋯⋯Wusa」\n\n'

### 'pinu'

- '嗎⋯⋯Pinu」\n\n'


## 00030_四章　魔王の躍動/040 POKER FACE

### 'piun'

- '！⋯⋯Piun⋯\n\n'


## 00030_四章　魔王の躍動/041 田原　勇　★

### 'lucky seven'

- '──　Lucky Seven\n大帝'


## 00030_四章　魔王の躍動/042 逐渐改变的村庄

### 'piun'

- '對不要Piun！」\n'
- '！⋯⋯Piun」\n\n'

### 'wusa'

- '不對，Wusa！」\n'
- '是變態Wusa」\n\n'

### 'wuza'

- '\n「Wuza！⋯⋯'

### 'azure'

- '魯」（Azure）⋯\n'


## 00030_四章　魔王の躍動/045 玛妲穆，前往温泉旅馆

### 'piun'

- '忙洗背Piun？」\n'


## 00030_四章　魔王の躍動/049 雪风，袭来

### 'labyrinth love'

- '迷宮，Labyrinth Love」\n\n'

### 'bubblicious'

- '是Bubblicious」\n\n'

### 'solid'

- '體級（Solid）的裝'


## 00030_四章　魔王の躍動/051 白天使与魔王大人

### 'piun'

- '去⋯⋯Piun？」\n'
- '吧⋯⋯Piun？」\n'
- '拿⋯⋯Piun？」\n'
- '路⋯⋯Piun？」\n'


## 00030_四章　魔王の躍動/052 湯煙の死闘

### 'gae bolg'

- '─能與Gae Bolg（貫穿'


## 00040_記録　于某间聊天室/NEO UNIVERSE

### 'neo universe'

- '年。\nNEO UNIVERSE。\n世'

### 'millennium'

- '音⋯\nMillennium\n無論'


## 00050_五章　恋の迷宮/057 監獄迷宮　一階層　★

### 'porter'

- '天就是PORTER！」\n'
- '就是以PORTER的身份'
- '那些是PORTER（譯：'

### 'nyaaa'

- '物──nyaaa！」\n'

### 'snow trolley'

- '板車/SNOW TROLLEY》」\n'

### 'ogre sword'

- '──　OGRE SWORD（譯：'

### 'ogre'

- '劍）\nOGRE（譯：'

### 'scaredpanther'

- '──　SCAREDPANTHER（譯：'

### 'scarlett'

- '寫錯了SCARLETT/赤紅'

### 'scar+red= scared'

- '遊戲的SCAR+RED= SCARED/紅傷'

### 'snow valentine'

- '──　SNOW VALENTINE（譯：'

### 'snow crystal'

- '的冰的SNOW CRYSTAL（譯：'

### 'dwarf'

- '、雖然DWARF（譯：'
- '術好的DWARF製作。'

### 'santa'

- '原文是SANTA/聖誕'


## 00050_五章　恋の迷宮/058 鬼之轟動與拉比村

### 'porter'

- '裡，而PORTER們則是'

### 'blik'

- '蘭語 BLIK，馬口'


## 00050_五章　恋の迷宮/059 田原の視察

### 'nine'

- '\n\n「NINE？？」'
- '個叫做NINE的集團'
- '會也是NINE的一份'
- '９」「NINE」等等'


## 00050_五章　恋の迷宮/060 親信們

### 'byeb'

- '說著「ByeB～'

### 'ye'

- 'Ye」揮著'


## 00050_五章　恋の迷宮/065 監獄迷宮　十二階層～十五階層

### 'pyon'

- '、喵、Pyon或是嗚'
- '：那個Pyon我實在'


## 00050_五章　恋の迷宮/068 魔王的歸還

### 'pyon'

- '您了⋯Pyon」\n\n'
- '木桶 Pyon」\n「'

### 'drill'

- '文是 Drill，所以'


## 00050_五章　恋の迷宮/072 風暴前2

### 'kevlar'

- '維拉（Kevlar）纖維'


## 00050_五章　恋の迷宮/074 魔王與魔女

### 'jug'

- '「嗯、Jug也累積'
- '被稱為Jug的數值'


## 00070_六章　魔王の進軍/00780_魔王軍の会議

### 'congratulations'

- '）\n\nCONGRATULATIONS──！'


## 00070_六章　魔王の進軍/00800_近藤　友哉

### 'hyper dry'

- '示著『Hyper Dry』的罐'

### 'entry malt'

- '註著『Entry Malt』的罐'


## 00070_六章　魔王の進軍/00810_併合

### 'collection'

- '用海軍Collection，用艦'


## 00070_六章　魔王の進軍/00830_黄金の神殿

### 'figure'

- '雨醬的Figure」\n\n'

### 'pyon'

- '了喲⋯Pyon」\n\n'
- '前囉⋯Pyon」\n\n'


## 00070_六章　魔王の進軍/00910_ゴルゴン商会

### 'spade'

- '英文是Spade，在撲'
- '貝特（Spade）阿，'


## 00070_六章　魔王の進軍/00930_這個男人是King――

### 'windshield'

- '風盾（WindShield），其'

### 'dragonquest ii'

- '以為是DragonQuest II阿！）'


## 00070_六章　魔王の進軍/00960_One Night Carnival

### 'enjoy vs chakkaman'

- '文是「Enjoy VS Chakkaman」，'

### 'chakkaman'

- 'Chakkaman在日本'

### 'air impact'

- '衝擊/Air Impact》」\n'


## 00070_六章　魔王の進軍/00970_下克上

### 'kingggg'

- '「Ｋ、Kingggg⋯你這'


## 00090_七章　夜の支配者/00040_同時多発大戦

### 'aja kong'

- '跤選手Aja Kong）\n\n'


## 00090_七章　夜の支配者/00090_ＭＥＳＳＡＧＥ

### 'congratulations'

- '。\n\nCONGRATULATIONS──！'


## 00090_七章　夜の支配者/00100_戦乱の兆し

### 'pyon'

- '好～的Pyon」\n\n'
- '紅蘿蔔Pyon」\n\n'

### 'wusa'

- '。吃吧Wusa」\n\n'
- '著幹勁Wusa」\n\n'
