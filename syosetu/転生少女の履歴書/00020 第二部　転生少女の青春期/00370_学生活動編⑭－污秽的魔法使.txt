「贵安，莉尤小姐。说起来，之前我写下名字的请愿书呢？如何？那个究竟怎麼样了？」

在午休的时候，卡特丽娜小姐带着萨洛梅酱来到了我这裡。

俩个人都带着午饭，因为有座位，好像是要一起吃的感觉。

对其他派系的刺客有点敏感的艾伦露出了险峻的表情，因为不管怎样都是喜欢一起玩躲避球的伙伴，并没有想要特地唱反调，正在观察卡特丽娜小姐的样子。

话说，不用那麼警戒也可以……

「请愿书的事情么……虽然你特地给我签名了，但是好像还是很困难的样子。」

我那样回答了，卡特丽娜小姐的表情扭曲了。

「啊！为什麼！这，我可是签名了啊！对吧！？」

「是的。」

怎麼说呢，虽然卡特丽娜小姐有点生气了，但事实就是事实。

对，有了你的签名还是不行呢。

虽然我也考虑过。

卡特丽娜酱有点大声地喊了出来，一些食堂的学生们的视线聚集了过来，渐渐地，在食堂裡，听到了「喂，看那裡的桌子。‘约定胜利的莉尤’‘必不可少的场地的管理者’‘挑战者艾伦’‘开战的钟声’、‘誘惑のサロメ’、‘人们都会聚集利兹’。好厲害！」这样的声音。

终於萨洛梅小姐和利兹君也有了躲避球玩家的称号了，话说在这个位子的全员，都有玩家名字了。

下级生们的热烈的视线有点吃不消。

各种各样的吃不消。

「嘛，莉尤酱！那样没有幹劲的态度就好了麼！这是我的！明明是写了我的名字的请愿书啊！那样却不被通过，这种的是不能允许的！」（注意，這裡卡特丽娜酱叫主角莉尤酱了，似乎是除光妈妈和艾琳小姐以外的第一个！）

在愤慨的卡特丽娜的旁边，萨洛梅小姐有些谨慎的搭话了。

「卡特丽娜，冷静点。因为是莉尤小姐的事情，一定有在考虑着什麼。再怎麼说也是‘约定胜利的莉尤’大人……噗。」

那麼说，萨洛梅小姐嘟哝着我的中二的名称，稍微开始噗噗的笑了。

萨洛梅小姐因为中二病还没有发病，听到了中二病的名称，有些想笑的倾向。

可恶，我的中二名称被戏弄了！

但是，萨洛梅酱也是，有了玩家名称！

请别忘了这边的人哦！？

「虽然校长先生的反应很好，但在那之前什麼进展好像也没有。比校长先生地位更高的人，也许有人不太喜欢我们的活动……如果有那样的人在的话，这裡就要借助‘诱惑的萨洛梅’的力量了，有希望可以把裡面的那位邀请出来的地方呢。」

我说出了萨洛梅小姐的中二名称，诱惑的萨洛梅小姐停下了笑，稍微皱了皱眉头看着我。

那个目光仿佛在说「快停下，不要用那个名称叫我」一样。

但是，一开始先说出对方的名称的是哪位呢？

是诱惑的萨洛梅！

在玩躲避球中的萨洛梅小姐，懒散的样子，看起来很有破绽，打算用球打她，要麼轻轻鬆鬆的闪开，要麼就能抓住球。

简直就像是在引诱一样，因为这样的传闻，所以是诱惑的萨洛梅。

噗噗。

「不、不行啊，萨洛梅，诱惑对方什麼可不行！真的不能做哦！」

虽然不知为什麼卡特丽娜小姐开始慌张了，嗯，只是随便说说而已。冷静一些。

「只是随便说说哦，卡特丽娜大人。而且那个请愿书的事情也没有放弃的打算。然後我想要问一下，卡特丽娜大人的古恩娜希丝的领地上，腐死精灵魔法使什麼的有么？」

「欸，腐死精灵……你啊，真的是很突然呢。嘛，虽然是可以回答你。我家的领地上，已经没有了。因为腐死精灵魔法使基本上都是特化腐死魔法的人比较多，领地上可以的话是幾乎不会有的。因为纺车的普及，都送到王都裡去了。啊啊，但是，因为夏洛特小姐是腐死精灵魔法使，所以你常在她身边么？」

那样说，卡特丽娜小姐将视线移向旁边的萨洛梅小姐好像在确认一样。

「是这样呢。嘛，大概，夏洛特小姐也是，在这裡毕业之後，我想也会就这样被送到王都去么……因为也是魔法使，在王都的税率也会上升。在这数年间，腐死魔法使的工作的纺纱，因为纺车的普及而变得没有工作了……不管哪家都为了降低税率而将他们送回王那裡去。」

那样说，稍微有一点同情的视线看向了夏洛特酱，萨洛梅小姐补充了。

夏洛特酱也，对会被送回王都这件事，是这样吧，似乎是这样认为的，做出了类似(´･ω･`)（失落）的表情（原文是ショボーン，查了後就是那个颜表情……），不过好像并没有受到很大的打击。

这样啊，意料之外的，腐死精灵使……不受待见呢……

纺车的普及的原因吧……确实腐死精灵魔法使在纺车和织布机普及之前，用魔法制作着羊毛之类的原料的丝，用魔法来织布之类的呢。

……夏洛特酱，那个，总觉得，对不起呢。

「顺便一提，送回王都的腐死精灵魔法使怎麼样了呢？」

我那样打听了，卡特丽娜小姐回答了我。

「谁知道呢，不就是普通的生活么？虽然被叫做是污秽的魔法使而被讨厌了，但魔法使还是魔法使。在哪裡的领地，也有在雇佣他们而把他们带走的事情。嘛，那样的话，虽然税率幾乎没有上升。」

坐在我旁边的夏洛特酱嘟哝着「污秽的魔法使」，越发失落了。

欸、那个，你还好麼？

嗯？

卡特丽娜小姐也是，你看，本人就在附近啊，再稍微，婉转一些就好了。真的。

嘛，但是，多亏了你，知道了想要知道的事情了呢。

在这个国家，还没有「发酵」一类的概念。

一部分红茶之类的发酵食品是流通的，不过，那是到现在的生活中，偶然诞生的东西，那个，就是发酵……也就是说。

「好的方向的腐烂」而诞生的东西，没有这麼考虑过。

只考虑着「到现在为止，就这样做」差不多吧。

虽然说那是托你的福，但也许能使出发酵一般的神棍魔法的腐死精灵魔法使很多都到王都了……

我正考虑着有各种各样的想要尝试的东西，艾伦向卡特丽娜小姐搭话了。

「喂，卡特丽娜。因为你说了污秽的魔法使之类的话，夏洛特总觉得十分失落。注意一些。既然本人在这裡，就不要说污秽的魔法使了。」

虽然艾伦罕见的感受到了他人的心情并想要保护她，但艾伦第二回说出了污秽的魔法使这个事，希望你能知道夏洛特酱的心情更加低落了这件事，要注意些哦，艾伦。

不愧是读取气氛的种子选手的優胜候补利兹选手考虑到了夏洛特酱的心情，「夏露因为也可以使用一点水的魔法，所以没问题哦」温柔地补充了。

大家都要向利兹前辈学习哦。

我正在向利兹前辈学习，在那旁边，艾伦和卡特丽娜小姐之间的气氛很险恶。

「什、什麼啊！并没有特别的恶意说的所以没什麼关係吧！！而、而且，我可不想被你直呼名字！」

「本来，你，去年找夏洛特的事了吧？那件事道歉了麼？」

被艾伦那样说了，卡特丽娜小姐带着有些痛苦一样的表情开口了。

「才、才不会道歉，但是……别想错了！我，并没有做什麼特别不好的事，只是稍微指导了一下而已！对吧，萨洛梅？」

然後，求助一般把目光转向萨洛梅小姐。

萨洛梅小姐，好好地嚼着口中的沙拉，冷静地看着卡特丽娜小姐。

「卡特丽娜，我已经对之前找夏洛特小姐的事，说得过於严厲这件事表示道歉了。」

「诶诶！！」

和千金小姐不一样的感觉，惊讶的表情面向了夏洛特酱，那是事实么？

用眼睛诉说着想知道这件事。

夏洛特酱总觉得有点不好意思的开口了。

「啊，是的。萨洛梅小姐前来道歉过了，同时也收到了点心。」

夏洛特酱证明了那个事实，卡特丽娜小姐张大嘴，看着萨洛梅小姐。

「为、为什麼，对我都没有说过！如果被邀请的话我也……！」

被萨洛梅小姐用笑颜说的卡特丽娜小姐苦涩着脸，交替看了萨洛梅和夏洛特酱，最後仇恨一样的看向萨洛梅小姐的方向。

「什麼啊！什麼啊！真是的！萨洛梅坏心眼！」

那样说完，这次又用着苛刻的目光盯着夏洛特酱。

真是的卡特丽娜小姐，这麼盯着的话，夏洛特酱不就变得吓了一跳么。

一边盯着，颤抖着嘴唇，卡特丽娜小姐开口了。

「夏洛特小姐，这个，那个……抱歉啦。」

十分小声。仿佛听不见一般的感觉。

我想是约定什麼的么，「欸？卡特丽娜大人说了什麼了麼？听不见啊。」（萨洛梅）

那麼说，脸变得很红了。

「所以说！我说了抱歉了啦！真是的！这样就行了吧！真是的！」

说着，视线返回自己面前的午饭开始バクバク的吃起来。

不像女士那样（优雅），贪婪的吃着，很不体面哦。

夏洛特酱看着怒气冲冲的卡特丽娜小姐，不知所措。

嘛，不用在意没关係的，夏洛特酱。

大概，是在害羞着吧，她，有点傲娇呢。

「那个，不，我才是，多亏了卡特丽娜大人，才能和莉尤大人他们相识，那个，谢谢你。」

纯洁的少女的夏洛特酱那样说了，浮现出充满慈爱的微笑。

夏洛特酱也太善良可爱了吧？

难道不是麼？

太过善良可爱的夏洛特酱的微笑震动到了我的心，「啊，哎呀！嘛，就是这样啊！你这不是很明白么」，仿佛因天使夏洛特酱的慈悲而得救的卡特丽娜小姐满足的微笑了。

怎麼是那种态度啊，卡特丽娜小姐。给我更加崇拜啊！没看到她身後發出的（圣）光么！

……啊啊，不可以的，把夏洛特酱的神圣的气氛当真，不就和タゴサク先生一样了麼。

因为看着那样的卡特丽娜小姐的样子好像很有趣的样子，萨洛梅小姐向夏洛特酱搭话了。

「话说，夏洛特酱，今年的长假期，要回古恩娜希丝的领地么？」

长假期……这样啊，已经，是这个时期了麼……

「不，这次也打算在王都裡。家裡的人都住在王都。虽然也想见村裡的大家，但是，从学校毕业的话，大概是不能回到领地了，我想太过於亲密也会变得很辛苦……」

那样说着的夏洛特酱，又一点悲伤的样子。

话说刚才，说了腐死魔法使基本上，为了降低税率都会被送回王都呢。

原来如此。和领地的人过於亲密的话以後会很辛苦么……

「对了，虽然想一起坐马车回去，但是有点勉强呢。莉尤小姐要怎麼办？」

「欸，我么？等下，还是要回卢比法尔吧。」

我那样回答後，到现在为止都在午餐的汉堡肉上忘情的艾伦，啪地一下面向我的方向。

「欸！？今年也顺便去一下吧？」

啊啊，那样说来上一次的长假期也在雷恩福雷斯特快乐地度过了一段时光。

也想和艾伦他们的妹妹素姬酱见面。

但是，今年，嗯~

「不好意思，今年也许有点困难。」

「为什麼啊！」

部下一副受到了冲击的表情，那样诉说着。

虽然你期待首领的到来我很高兴，不过今年在卢比法尔想要做的事情有很多呢。

比起那个，有希望夏洛特酱听一听的事情。

我适当的劝解了手下，把手放在了被说为污秽的魔法使，将要被赶出领地而失落的夏洛特酱的肩膀上。

「虽然有些突然，那个，夏洛特酱，这次的假期有空么？有想要你帮忙的事情，是除了夏洛特酱之外的都做不到的事情。」

我那样说了，一开始很吃惊的夏洛特酱她，「除我以外做不到的事情，么？如果我可以的话……」说着，点了点头。
