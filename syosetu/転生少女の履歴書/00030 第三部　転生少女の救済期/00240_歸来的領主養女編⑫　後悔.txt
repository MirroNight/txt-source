我被光妈妈抱着在地面上猛烈地翻滚了数圈。
停下来后，我抬起头确认情况。
光妈妈面色痛苦，呼吸急促。侧腹上部染上了一片赤红。
这个，是血？为什麼，血会……。

「莉尤，莉尤酱，後面……魔物，还在……」
光妈妈的口中發出了犹如呻吟一样的声音。
我内心动摇地看向後方，刚刚认为已经打倒的魔物，摇晃着幾乎已被砍掉的头部，摇摆不定地立起双手双脚想要站起来。
当看到魔物的右爪时我不禁一愣。那裡一片赤黑，还沾着几片光妈妈衣服的碎片。
直到此刻，我终於理解了。那个没死透的魔物，想要用那只钩爪攻击我。而光妈妈则在钩爪下保护了我。

「最、最後一击……！」
「我来做！」
我出声阻止了那麼说完就打算起身的光妈妈。

但是，首、首先要止血……。对，必须要止血……。
这样想着，我从衣服上撕下一块布条，正当我为了确认伤口的深浅，把手放到光妈妈侧腹上时，光妈妈连同布条一起，一把抓住我的手，自己将布条缠上自己的伤口。

「我的事这样就好，魔物……快点！」
被光妈妈这麼一说，我吓了一哆嗦，好不容易才点了下头，走向魔物。

我在思绪混乱中咏唱起增加力量的咒文，迅速砍断了还在抽动四肢，没能完全起身的魔物的手筋脚筋。
尽管魔物还在活动，但只要有咒文之力的话，也不是多麼困难的作业。
只是，刚刚一瞬间瞥见的光妈妈的侧腹伤口，一直深深烙在我的脑海裡……感觉自己现在的行动非常缓慢，心中焦躁不安，呼吸急促。

我想办法封住了魔物的动作後，拖着颤抖的脚回到了光妈妈身边。
虽然还没看到伤口的状态。但是那只魔物的爪子非常锋利，而且巨大。

光妈妈以仰躺的姿势，自己用布捂住了伤口。看上去已经精疲力竭，但是还有呼吸，胸口还是上下起伏。
可是，血还没止住。光妈妈衣服上的血迹正在不断扩大。

「光妈妈，请让我看下伤口！快点！」

光妈妈移开了按住伤口的手。
说不定运气好只是个小伤。因为是光妈妈，或许能勉强避开要害。
而且之前，光妈妈还準备自己去处理魔物，或许刚刚我一瞬间看到东西，只是我的错觉……。

我用颤抖地轻轻抬起光妈妈的手，确认伤口。

看到伤口时，我哑然失声。
伤口……太大了。
已经实际伤到了内臟。

不管怎麼想，这都不是现在手头的治疗道具能处理的伤口。
我再次撕开衣服，为了止血压住伤口。光妈妈發出了痛苦的呻吟。

但是，我知道。即使这样做也止不住血。伤口太大了。

快点思考。该怎麼做才能得救。就算进行止血处理，那种伤口……已经，怎麼做都没用了。可是，不快点止血的话，就会失血过多。

魔法。只有用魔法才能得救。

「光妈妈！咒文！你可以咏唱咒文吗！就是我之前给你看的治癒魔法！」
「抱歉，莉尤酱……还，念不出咒文……」
「我现在就咏唱咒文，请跟着复读一遍！」
我咏唱咒文。光妈妈勉强念出了前两个文字，可之後就张口难言。随後，光妈妈摇了摇头。
「不行……。念不，出，来……」
我向光妈妈坦白魔法的事情後，还没过去多长时间。
据说通常情况下，不管相性多好，要掌握咒文都要花至少半年时间。
况且，最近一直忙的不可开交，根本没有练习魔法的时间。

更早一点。要是我更早一点向光妈妈坦白魔法的话……。
啊啊，但是，就算现在再想这种事也无济於事。必须找到其它办法、其它的……。
「没、没问题，我一定会治好光妈妈！所以！」

然而，我想不出其它任何办法。
我连思考的时间都不想浪费，从第一首短歌开始咏唱咒文。

全部，全部都咏唱一遍。绝对会有治疗他人的咒文。虽然之前一直没有发动，但绝对会有！
我一心一意，在心中祈祷着治癒光妈妈的伤口，同时从第一首短歌开始咏唱……。
即使之前一直没有成功过，但现在我只能依靠它了！

拜托了……！帮帮我……！

我带着祈祷的心情不断咏唱咒文。
能治好光妈妈伤口的咒文、咒文、咒文。
然而，不管咏唱哪首短歌，都无法治疗伤口，反而是血液还在不断流失。
咏唱咒文时發出的朦胧光晕也仅是围绕在我的身边。

为什麼，为什麼，为什麼，为什麼。


讨厌，讨厌，讨厌。


明明我和光妈妈之间的气氛还这麼不融洽，而且我之前都说了什麼……？
说光妈妈是死脑筋，说了那麼自大的话，让光妈妈担心，生气，还不知悔改，得意忘形……。
我不想让那些成为最後做的事，绝对不要。

我不顾一切地咏唱记忆裡的短歌，最终，所有有可能治癒他人的短歌都咏唱完了。
可是，治癒魔法仍然没有发动。
到底要怎麼办才好。接下来要做什麼。究竟要怎样才能治好光妈妈。
缠在光妈妈身上的布条已经被一片鲜红。到底流了多少血……。
我的脑海中闪过被削去一块肉的光妈妈的侧腹。

「为什麼！为什麼你要挡在我前面！我，我……！」
我不再咏唱短歌，从口中爆發出责备光妈妈的话语。
我根本没想过要说这些话，我这个笨蛋，但是，停不下来，讨厌，我讨厌这样！

「因为，我是，莉尤酱的妈妈，所以……」

「我讨厌这样。光妈妈不在的话，我也活不下去！不要！讨厌，我讨厌！」


「过来，莉尤酱……摆出那种表情的话，可爱的脸都浪费了。莉尤酱，你一定没问题，所以」

「才不会没问题！光妈妈不在的话，我……」
突然涌上一阵衝动，话语堵在喉咙，我忍住了将要漏出的哽咽声。
不能哭。在这裡哭了的话，就等於默认光妈妈已经没救了……。

「嘴唇，流血了。这麼用力咬着嘴唇……不行哦。不要，露出，那种表情……」
光妈妈这麼说着，用右手轻轻抚摸我的脸颊。
嘴裡有血的味道。似乎太用力，咬破了嘴唇。但是现在根本没有空闲去管这种事。
好冷。光妈妈的手，非常的冰冷……。

「不要，不要不要不要。光妈妈，不行，我不要这样！」
忍住的眼泪渐渐夺眶而出。即使哭泣，呼喊也无济於事，但是，我仍然止不住地哭喊。该怎麼办才好。快点思考，思考，思考……。
「嘴唇，看上去，很痛。不行哟。不要咬着」
光妈妈用拇指拭去我嘴唇上的血痕。

「那种事怎麼样都好！最要紧的是光妈妈！光妈妈，都是我不好！拜托了，不要走。对不起，我会认错的！不管要说幾次都行，从现在开始，我会做个好孩子！光妈妈说的话不管什麼我都会听，我不会再傲慢自大了，我会给你帮忙！我，讨厌……。留在我身边，我，最喜欢光妈妈！光妈妈不在的话，我肯定也活不下去……」

「莉尤酱，没问题，的。……谢，谢谢。我也，最，喜……」

光妈妈的声音越来越小。我握住了失去力气後从我脸颊上滑落的手掌。

「光妈妈……！」
在被泪水浸湿的视线裡，微微看到光妈妈的眼皮慢慢闭上。

「不要！骗人！骗人的。为什麼，我只是……！」
我紧紧握住光妈妈的手。豆大的眼泪夺眶而出，浸满泪水的视界清晰了少许，於是，我注意到了正在發生的异变。

光妈妈的身体在发光……？
光妈妈的身体周围笼罩着一层光晕。就像是咏唱咒文时，围绕在我身边的光芒一样……。