
「关於那个啊……」

苍太从单肩包裡取出了药。

「虽然肝臟是没有拿到，但是已经和卡雷娜一起準备好药了」
「是、是怎麼一回事呢？虽、虽然能準备好药的确是一件好事，但不是没取到龙的肝臟？那麼，到底是怎麼制造药的？」

艾鲁巴斯为摆在眼前的药感到吃惊，也对明明没取到肝臟却可以制作药这件事感到惊讶。

「艾鲁巴斯，你知道石热病的特效药的素材是什麼？」

卡雷娜慢慢地询问。

「那是当然的。就是龙的肝臟、石化蜥蜴的爪子、圣水不对吗？虽然爪子和圣水已经準备好了……」

像是在表示说的不错一样卡雷娜点了点头。

「都记得了呢，正确回答喔。但是啊，那个药还存在另一种配方喔。使用那个配方的話就不需要龙的肝臟咯」

卡雷娜的話语使得艾鲁巴斯吃惊了。

「……没、没想到呐。感觉今天一直在受惊啊，也就是说索太殿下準备了另一份配方的药嘛」
「就是那样。关於另一个配方，作为龙的肝臟的代替需要的是龙の泪、爪子和圣水依然是必要素材，然後追加素材则是治癒之树的叶子3枚」

艾鲁巴斯变成一副目瞪口呆的脸。

「因为比起收集肝臟还是取得泪水比较容易啊。然後，配方会改变是为了这边的方便所以就请了卡雷娜协助制作药了」
「如、如果老朽没听错的話，刚刚是说了治癒之树的叶子吗？」

使了九牛二虎之力让头脑能运转後仅仅發出了那个质问。（炎阳：这句乱翻的～）

「是喔，这份配方直到百年前都还是普遍地被人使用着。当时的治癒之树的叶子可是比森林裡的药草还要容易取得的物品喔。在随着入手变得困难，人们也一一变得使用新配方了」
「是、是这样吗……不对，虽然是那样，但不是指那个呐……索太殿下是如何……入手那个治癒之树的叶子？」

卡雷娜的回答牛头不对马嘴，艾鲁巴斯好不容易地提出了原本想问的质问。

「关於入手方法虽然不能说，但至少那是合法入手到的」

「是那样吗……不对，是那样的吧。那个可不是随意就能入手的的东西呐」
「嘛，就是那麼一回事。我也不打算打听喔。那麼关於药，那可是的确能用的东西喔。因为已经用我的鉴定检查过了。今天就是作为那个药的保证人来的」

艾鲁巴斯边抚摸着鬍鬚边说道。

「原来如此，是那麼一回事吗……但如果是那样的話，关於索太殿下的报酬可要重新考虑呐」

苍太叹了一口气。

「哈，果然会变那样嘛。这边可是做了和委托不一样的事啊，虽说不是正规的公会委托，但还是违反条约了不是吗」

显得脱力，并抬头望向天花板。

「那可不对，怎麼会有那种事呢。老朽的目标是制作药後将孙女治疗好，仅管委托的确是要索太殿下取得龙的肝臟但那就真的是无视阶级的委托。也就是只要用这瓶药将孙女治疗好的时候就必须要增加报酬的意思喔」（炎阳：这句真是太臭太长了，想救就快点救啊，分秒必争不懂吗，说一大堆废話来幹啥）

「啊，是那样的事嘛。那样的話就没问题了，这边可是很担心会被判定为没达成委托呐」
「责备将治癒之树的叶子準备好甚至连药都制作好的男人的話可当不了领主呐，艾鲁巴斯也是个直率的人。道谢都来不及了，更不用说是谴责恩人」

卡雷娜帮忙说了几句話。

「素材只是碰巧持有着而已，不需要太在意呐。比起那个……那瓶药还是快点让你孙女喝下比较好不是吗？记得在罗患上石热病的期间会不断地发高热」
「说的也是。毕竟也得到卡雷娜的保证了，那就赶紧走吧。那麼，就请两位也一起跟来吧」

那样说完後，不知是不是因为担心起艾莉娜，艾鲁巴斯用手拿了药瓶後就立刻赶脚离开了房间。丹、苍太、卡雷娜也跟在艾鲁巴斯的後面。

艾鲁巴斯从快走开始渐渐地提高速度，最後变成了在奔驰。
三人也为了不被丢下而跑了起来。在途中擦肩而过的女仆瞪大眼睛地看着艾鲁巴斯等人。

抵达到上了楼梯後深處的房间门前後才终於停下脚步。
有点喘不过气的艾鲁巴斯进行深呼吸，调整呼吸。

「哈—，哈—，呼—，哈啊」

敲门後将脸贴近门并搭話。

「艾莉娜，是爷爷喔。今天把药带来咯，制作了那个药的朋友也一起来了，他们都是好人所以让他们也一起进去可以吗？」
「可以喔，朋友们也请一起进来吧」

从房裡传来回復後艾鲁巴斯慢慢地将门打开并走向房间裡。

在那个带有华盖的大床上，有一位蓝色头髮的少女躺着并只把脸面向了入口处。（炎阳：这裡艾莉娜的头髮的颜色原文为青い髪，并不确定为青色还是蓝色，个人认为还是蓝色会比较美，所以就翻成蓝色了，只怕到时出文库後被插图打脸…）

在额头上浮现着许多粒状的汗水。

「只能躺着打招呼真是十分抱歉，爷爷、丹先生、卡雷娜小姐和……在那边的大哥哥是爷爷的朋友吗？」

那份声音感到很虚弱。

「嗯，是喔。是一位冒险者，名字叫索太。就是他带来可以治疗艾莉娜的药喔」
「嘛，把药带来了？这个病的药不是很贵重的吗……对不起，给大家添麻烦了」

艾鲁巴斯因为艾莉娜说的話而变得狼狈。

「在、在说什麼呢。艾莉娜不用在意呐，都是爷爷擅自做的事呐。要向大家道歉的話只要老朽一个就足够了」
「不对，请让我来道歉吧。而且也给爷爷添麻烦了……要準备好药也是很辛苦不是吗？」
「如果是为了艾莉娜的話不管是什麼事都不会辛苦呐！」
「但是……」

看着两人没完没了的对話，苍太插嘴说道。

「呐，要不要道歉这种事就先不用管了，不赶紧喝了那瓶药吗？在那样的状态下说話也很辛苦吧」
「有如索太所说，不管是谁错都好还是请尽早喝药吧。不那样做的話我们把药制作了出来不就没意义了嘛」

丹也在两人的身後不断点头。

「也、也是呢。那麼，这就是药喔。虽然可能会有一些苦但要忍住喔」

打开瓶盖，然後往艾莉娜的嘴边伸去。
这次艾莉娜也什麼都没说就这样把药喝了下去。
因为强烈的苦味而闭上了眼睛，皱起眉头并依旧把整瓶药都喝了。

紧接着披上了被单的艾莉娜的脚尖發出了光。
赤红的脸也逐渐消退，额头上的汗也都不见了。
在光消失後艾莉娜也睁开了眼。

「怎、怎麼样？身体的状态如何」

艾莉娜以依然躺着的姿势有时动了动手，有时动了动脚。

「爷爷……治好了，治好了哦！身体也不会感到痛苦，脚也能动咯！！！」
「真的吗！！太好了，真是太好了，艾莉娜……」

艾鲁巴斯一边流泪一边抱紧了艾莉娜。被抱紧的艾莉娜也热泪盈眶。

互抱了一阵子後艾莉娜稍微放鬆了手。

「爷爷，我想站起来走路。因为已经躺了两星期了啊」
「哦哦，是吗。要小心喔，毕竟是大病初愈呐」

艾鲁巴斯离开了床边。

「嗯，我知道的」

那样回应後艾莉娜从床提腰并试着站了起来。

「啊！」

让身体稍微向前倾斜，抬起臀部，并在要完全伸直膝盖的下一个瞬间，还没脱力就已经倒了下去。（炎阳：这裡不是很明白在说什麼就直译了，有兴趣的人可以试试这是什麼动作）

「喂喂，很危险啊」

那身体不知是幾时移动的，被来到前方的苍太抱了起来。
然後，让自己再次坐在床上。

「记得你说已经躺了两星期了吧。躺了那麼久即使有多年轻都好肌肉也会变瘦的，就是要不要慌张慢慢地进行复建呐」
「知、知道了。十分抱歉」

因为没有与祖父和家裡的人以外的男性接触过，所以艾莉娜的身体因为紧张变得僵硬。

「老先生，因为还是小孩子所以好动是没办法的事，但是身为大人的你不去阻止可不行啊。就和你说的一样是大病初愈呐」
「对、对不起。还有真是十分感谢，不管是刚才的事还是药的事都很想和你道谢」

艾鲁巴斯深深地低下了头。在看了那个以後艾莉娜和丹也低下了头。

「嘛，治好了就是件好事呐。比起那个报酬的事……就明天再来处理好了，今天你们两就悠闲地聊天吧。明天的午前还会再来的，卡雷娜也是没问题吗？」
「我只是为了保证药的效果才跟来的，明天不会来的。报酬也不需要啦，很久没做过那麼有趣的东西所以那样就足够了」

苍太耸了耸肩。

「真是无所欲求呐，那麼明天就我一个人来吧。那麼再见咯」

说完後不待回復就返身走出了房间。

「那个，真的是非常感谢！！」

艾莉娜大声地喊道，但是并未回头而是举起右手作为回应。

-完-

