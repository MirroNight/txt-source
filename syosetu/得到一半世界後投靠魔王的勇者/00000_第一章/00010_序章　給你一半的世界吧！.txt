
魔王，很开心地笑着。

[库库库……勇者哟，与你的战鬥真的很愉快哒！把这场战鬥若是最後，真的有点可惜呢]

顶着魔王的名号，然而却是可爱外表的幼女，不过这麼看来她就是恶毒的魔族的王呢。
被将世界陷入恐怖，割取希望散布绝望的她给认同的事实，让我很高兴。

[没错呢。我也是，与你的战鬥时最棒的……真的是，认为这是最後的話很遗憾]

魔王城，大厅裡。身为勇者的我，对魔王的話语点头，以肯定态度回应了。
受石化，我并不讨厌魔王。虽然这傢伙是我的敌人，但却是数次交剑的关係……不是性格败坏的傢伙，这种程度的事请早就知道了。
不如说这傢伙是很好的王。比起我们人类的猪一样的王，简直好太多了。真的是，魔王有着很好的人格呢。
正因如此，这个时间的到来才令人觉得遗憾。

[勇者与魔王的，最後的战鬥……么。真是讽刺呢，明明对我来说你是完全不觉得讨厌的。这就是所谓的命运么？]

魔王的讨伐。被教育这个任务的我们勇者队伍全体，在这天向魔王城發起了决战。魔王也回应了，全力迎击了我们的挑战。
勇者，魔法使，僧侣，武斗家，战士，构成的我的队伍，死鬥的最後，终於来到了这裡。
为了与魔王本人的了解，同伴吧我送到了这裡。
漫长的旅途，在这裡终於要结束了。
然後，与因缘深厚的魔王……无论哪一边胜利，就此分别是无可厚非的。
相互理解了这件事。最终决战之前，稍微有些感慨我想也是没办法。

[说的是呢。我与你，实在是好对手……实际上，比起部下的谁，我更喜欢你呢]
[……哦，哦]

不过，魔王那边的样子稍微有点怪。一直以来的让人受不了的喜欢战鬥，以及那麼说着地一副无畏的笑容哪裡都见不到。
现在的她，以认真的眼睛——对我，一直注视着。

[呐，勇者哟……在我政府世界的时候，分给你那其中一半吧]

她毅然地放出了話语。

[作为代替，成为我的部下吧]

那麼说着，魔王向我伸出了收。虽然是小小的手，但那只手能产生多大的力量，我可比谁都清楚。

[给你世界的一半，所以不来与我一起么？]

那样的她的愿望，是作为勇者的我所不可能接受的。

[——]

回想起来，至今为止的道路。
有许多的苦难。有数不清的不讲理。
即便如此，也有分享笑颜的伙伴在。对我抱有期待的人有很多，所以。
背叛大家什麼的——是做不到的。


我也有这麼思考的时期呢。

[真的？给一半么！？太好了，愉悦吧，和你联手了！]

对於魔王的诱惑，我败了。
因为，那可是给我一半的世界哦。不要的理由我想不出来。

[什……么]

我接受了魔王的愿望，然而不是他人，而是魔王本身震惊了。

[你，勇者的骄傲呢，义务呢，这麼做……可以么？]

不知为何开始说前後不搭的話了。明明只是个魔王，那种事情才不用担心呢。

[骄傲？那是什麼好吃的么？义务什麼的才不知道哟。再者说了，我才不是想当勇者才去当勇者的。还是被强迫当勇者的]

没错。我并不是，想当勇者才当上的勇者。
从小的时候开始就被当做勇者，与我的意愿无关被锻炼了。
只是凑巧有才能，勇者poi的力量被發现了，才被当做勇者而已。

[而且，假如把你打倒了，那之後我能得到什麼？不是只有『拯救世界』的名誉
么？名誉不能让肚子填饱啊！比起那种东西，得到一半世界的那边更让人开心]
[不，不是……但是，报酬也不只是那样的东西而已。从周围给予的评价，权力，想要拿到手的东西什麼都可以拿到手的……答应我的劝诱，真的好麼？]
[啊啊，可以哟。报酬什麼的，比起世界的一半就只是微乎其微的东西……而且我，与你联手。不对，想与你联手]
[同，同伴的話，不是会悲伤的么？]
[那些傢伙的話不可能悲伤的吧！！]

我在这裡，發出了今天最大的声音。要说为何，因为现在的一句話，从我的脑海裏唤醒了某个事实。

[你知道吗？我，其实撒……与僧侣是青梅竹马啊。为了与她结婚，这场旅途才努力过来的。但是啊，僧侣她啊，实际上是已经和魔法使交往了啊]
[啊……]
[察觉到了吧？没错，我可是碍事虫啊！不仅如此，战士和武斗家也是，实际上是瞒着我在交往着的啊！？讲真的啊，队伍裡已经没有了我的容身之处了啊]

一旦说出来，就停不下来了。我不知不觉的，像是把压抑在心中的郁愤爆發出来般，对着魔王叫了出来。

[这个最终决战也是，感觉上正是同伴们牺牲自己让我前进的，但是事实完全不一样啊！？在魔王城内，他们把我丢下来不知道去哪裡了，因为没办法才一个人前进的啊！！]

没错。同伴什麼的，全都死掉算了。丢下我变成现充什麼的，那样的才不是同伴啊。

[而且啊，至於对我『期待着你哟』这麼说着让我拼命幹活的公主様啊，那傢伙也只是个**而已啊！？可恶啊，成天讲着矫揉造作的話……不觉得让人有多麼的期待么？我啊，以前可是为了公主様的笑容去战鬥的哦？现在想起来，都想杀死过去的我了]

对於那个觉得我很好使的公主様，她的脸我再也不想见到，都积压了这种程度的恨意了。
僧侣和公主様，将来和哪位结婚呢--，之类的……索性两人一起结婚什麼的，考虑着愚蠢的事情的过去，不如说已经是黑歷史了吧。

[保护了的民众，因为我的脸不起眼就把我当成笨蛋。明明是勇者，真普通（笑）之类的，多餘的担心啊呆子！！暂且不说，同伴全都是美男美女。明明我比谁都还努力，就只有同伴受到了评价，这种时候也有呢？已经是，我是为了什麼而努力的，完全不明白了啊]

之後，我注意到了。
我什麼都没有了。应该守护的东西，本应能得到的幸福，全都只是幻觉而已——这些，让我注意到了。

[对我来说，只有魔王是治癒的。无论什麼时候遇见，你都没有改变。能认同我，与我全力地战鬥，那真的是很快乐。那样的你，我想要得到手]

因此，对於传达过来的认为我是必要的魔王的心情，打从心底感到高兴。
对於认同我的她的話语，让我深深的感动了。
抓住伸出来的手，我对魔王跪下了。
对着那样的我，魔王……哭了。

[呜……勇者哟，你经歷的人生真是很艰辛呐]

看起来，好像被同情了。
那份温柔，渗透进心裡。就是那样啊，魔王……我经歷过的人生真的是很艰辛呐。

[已经可以了。没问题了……在我的身边，变得幸福吧！这是命令哒，你与我联手後，不绝对遵守可不行的使命哒。那个，能否做到？]
[魔王的，能待在魔王的身边的話，仅仅如此我就能变得幸福]
[那是当然哒！待在我的身边就行]

然後，魔王温柔地拥抱了我。
明明只是凶恶的魔族的王，但她的身上却有很香的味道。不止如此，那个身体还很柔软……对着那样的她，我决定誓与忠诚。
虽然是勇者，但从魔王那得到一半的世界後背叛了。
在她的身边，我会变得幸福——


——完——

