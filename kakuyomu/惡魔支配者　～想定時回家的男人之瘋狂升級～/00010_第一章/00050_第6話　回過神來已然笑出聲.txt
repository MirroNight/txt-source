
　调整凌乱的呼吸後，喜悦之情逐渐涌现而出。
　这并非是胜利的感慨，而是幾分钟便赚取到一千五百日元的欢喜。
　根據方才的战鬥，即便是金属棒也足够打倒饿鬼。在紧要关头，神乐还会使用魔法。
　这不是能行得通吗，他露出满足的笑容。

「如果是刚刚那种战鬥应该就没问题吧」
「虽然我不希望主人参加战鬥就是了」
「别这麼说嘛，重点是还有饿鬼吗」
「嗯~在异界裡面探知範围就会变小啦……在那边有反应喲」

　往神乐所指的方向前进。
　那边是在穿过便利店停车场後的地方，也是每天的上班路线。不过由於异界化的缘故，气氛截然不同。
　不分昼夜总会有人通行的人行道上杳无人烟，车辆来往频繁的公路上仅停放着几辆像是遭到抛弃的汽车。旁边的便利店既没有顾客也没有店员。
　无论是何处都毫无人影。也许正是因为如此，这裡就像是陌生的地方；或是说犹如SF小说裡描写到的从某瞬间开始人类全数灭绝的城市一般。
。
　重新意识到这种异样光景，使得亘不由得吞了吞口水，而神乐则是在他耳边低声细语道。

（饿鬼就在那辆黑色的大车後面，小心一点）
（好，交给我吧）
（诶、等一下。所以说如果主人都参加战鬥……啊啊，真是的！）

　亘慎重地走向黑色面包车。途中刻意發出脚步声之後，饿鬼便从车子的阴影裡飞奔而出。
。
　尽管它步伐缓慢，不过在不知情的情况下说不定会遭受偷袭。然而亘事先得知此事，已经充分做好战斗装备。
　在第二次看到它那令人毛骨悚然的模样以後也就不怎麼吓人了，亘摆出突刺的姿势往前猛冲，就这样刺中饿鬼肋骨显露的胸口处。
　气势加上体重差距，使得体格较小的饿鬼被击飞出去。他再往头部挥下一记，趁着饿鬼倒在地面时又胡乱地进行攻击。

「哈！」
「啊啊，我的出场机会又没有了……」
「来吧来吧来吧，来啊！」

　神乐双手按住自己的嘴角，仅仅是毫无出场餘地的眺望着钝器响起的暴行。
　另一方面，亘凭着旺盛的热情逐渐挥舞着金属棒，其心头战鬥以外的事情时隐时现。
。
　整天总对着电脑工作，毫无自由时间。扼杀自己的感情忍耐上司的捉弄与无理取闹，听着後辈相亲相爱的工口話题；同期的同事逐个结婚，酒会上老是因为自己单身而被他人调侃。明明努力地想要受欢迎，结果却依旧没有成果。
。
　至今以来都将嫉妒、怨言、不满扼杀在内心深处，而只要将这些情感全数投入反覆挥舞金属棒，那每一击都将得到升华。

「库、库库、啊哈哈哈哈！」

　回过神来他已然笑出声，殴打着饿鬼。
　由衷尽情地欢笑已然是时隔几月、不，幾年了呢。再确认到手机所回收的DP，他的笑容更加灿烂。
　偶然瞥了一眼，神乐的神情就像是目睹某种惊悚的场面。

「哈哈哈，这还真是让人欲罢不能啊喂」
「主人真的是……不，现在不是说这件事的时候。听好了，下次是由我来战鬥 主人是不能战鬥的啦」
「是是是」

　看样子神乐是对於自己没有出场感到不满。即便她出言抱怨，亘也只是随意敷衍，左耳进右耳出。
　後来依旧持续暴行。在發泄压力与额外收入面前，已经没有什麼可以阻止得了他。
。
　若不想让亘战鬥，其实神乐只要不将饿鬼的位置告诉他就行。不过身为从魔，只要受到命令就必须服从不可。可这样一来，无论是提醒还是抱怨都无法阻止亘。
　神乐只能眼巴巴地看着亘手持金属棒，愉悦地杀向饿鬼。拜此所赐，神乐已经完全闹起了别扭。

「够了，真是够了，反正像我这样根本就……不过下次真的就由我来打倒吧！」
「嗯，差不多十一点了，是时候该回家了」

　神乐鼓起幹劲，然而她旁边的亘看了下手机後嘀咕道。娇小少女受到打击，脸色大变。她死缠烂打地说道。

「诶~！别这麼说嘛。继续战鬥吧，接下来才是我活跃的时候」
「不行，要是留得太晚就会影响到明天工作。赶紧回家吧」
「呜—，要是主人都战鬥就没有我的存在意义了啦，明明我也能战鬥」

　神乐喋喋不休地说出自己的不满，然而亘并没有听进去。

「下次还来就由我来战鬥。明白了吗，绝对要让我来」
「关於这件事我会妥善处理，而且一小时打倒八只，共24DP吗。换算成日元就是一万两千，考虑到这是时薪可就是一笔不少的赚头」
「诶~主人要将DP全部都换成金钱吗？DP可以购买我的装备等之类的，我还想买点这类东西呢」
「嚯，还有这种功能吗」
「是呀，还有各式各样的功能啦。主人真是的，你好好确认才对」
「毕竟我是不会看说明书的那类人」

　亘说着借口，啟动程序。
　从菜单界面上试着点击商店。然而显示出『準备中』。

　从下载後到现在，由於他没有认真啟动过程序，因此晚了许多才注意到菜单界面上还有『人物状态』『技能』『道具』。

　他实在说不出口，自己满脑子只想着DP换钱，因此没注意到这些。
　亘尴尬地咳嗽一下，点击人物状态。

…………………………………………………………………………
名字：五条亘　种族：人类
等级：２　经验值：２４　持有ＤＰ：２４
…………………………………………………………………………
Ｎｏ．１
名字：神楽　种族：皮克西
等级：２　经验值：２４　技能点数：２
ＨＰ：６／７　 ＭＰ：２／１５
技能：探知、雷魔法（初级）

…………………………………………………………………………

　上面不断罗列出文字与数值。不过神乐那边的内容比较多，而且还显示出娇小可爱的人物像。

「嚯，居然还有等级和技能点数吗。啊，神乐的种族是皮克西吗」
「是呀，种族就是皮克西啦。啊啊，我的等级提升到2了，太好了！」

　神乐见到亘手上的手机显示出的等级，發出喜悦的声音。
　如此接近的距离使得亘不由得心中一阵悸动。
　尽管只是二次元的经验，不过这不就是关係良好的女孩子意想不到地接近过来，因而感到为难的青春一幕吗。
　在欢喜与害羞的交错之下，亘感到不知所措。神乐回头仰望，意识到投向自己的目光後歪着头。

「主人，你怎麼了？」
「没、没什麼」
「哼~是吗。比起这件事来，时间没问题吧？」
「对了，嗯，我们回去吧」

　亘连忙将手机收起来，前往来到异界时的地方。
　神乐与进来时一样在同样的空间上敲了下，而空间又再次摇曳起来。

「那我们回去吧」
「是啊……等一下，如果回到原本的世界与其他人碰面不就糟糕了。突然出现一个人肯定会引發大骚动吧」
「没事的啦，不知道异界存在的人们即便看到别人出入这裡也会视而不见」
「真是意义不明……不，等一下……是这样啊」

　亘蹙起眉头，回想起某件事情。那就是人类无法认知到在自己认知以外的事物。
　 幕府末期欧美帆船来临时，江户时代的人们并没有在意眼前的黑船。由於他们对於黑船这种未知的存在毫无任何概念，因此并没有足够的认知到。
　尽管不知道这件事是真是假，不过以脑科学而言是十分有可能的事情。
。
　以此类推，对於那些不知异界存在的人们而言，或许是无法认知到出入异界的人士。
　虽然说不定是其他理由，不过总之就先这样解释吧。反正异界、DP之类的就已经搞不懂了。事到如今再来思考认知怎麼怎麼样也是无济於事。

「也就是说有某种东西在妨碍别人的认知吗」
「大概吧，至少从异界出来以後，一时半会是不会察觉到才对」

　对於神乐所说，他点点头，随後一同穿过摇曳的空间。
　这一次他尝试睁着眼睛通过这裡，仿佛就像是穿过透明单薄的膜块一般，景色骤然间显得朦胧不清，紧接着在转眼间就变为漆黑的夜晚。在适应黑暗之前，一切都显得乌漆墨黑。
　相反，声音纷纷传播过来。
　车辆行驶的喧嚣声，空调室外机的运作声，不知从那户人家传来的电视机的声音与笑声。耳朵聆听着平淡无奇的日常声响，心头才涌起平安无事回到这裡的实感。
。
　眼睛习惯黑暗，呼吸调整完毕後，他才察觉到近距离走过来的男人。
　彼此之间近到想藏匿起来都无法实现。亘连忙将手上的金属棒藏在後头。

「啊，晚上好。虽然我是这幅打扮，不过可不是可疑人物」

　在人烟稀少的暗淡巷子裡，若有个男人头戴头盔，手持金属棒，别人应该会發出惨叫逃之夭夭吧。要是报警还会上新闻，直奔无职路线。

「那个，咦？」

　然而对方完全没有反应，快步行走的步调依然没有变化。仿佛像是面对路旁的石头一般，毫不在意这边的样子。
　正如神乐所说，他并没有注意到亘。这应该就是认知遭到妨碍的状态吧。

「真的耶，完全没有注意到的样子」
「我说得对吧，既然知道就回家吧」
「也是，毕竟不知道这种状况会持续多久」

　亘理解到认知妨碍正在运作，走了起来。
　初次异界搜索结束，他压抑着无法冷静下来的兴奋之情回到公寓，於是回归到平时的生活当中。
