
1

收在内侧口袋的魔法道具振动，克莱姆把它拿出来。

这是个大小能收在手心裡的怀表，表盘刻著三根针──时针、分针与秒针──以及围绕这三根针的十二个数字。

有的大型时钟会采机械式，不过个人携带的尺寸，在王国就只有魔法道具。由於钟表与生活有著密不可分的关係，以魔法道具来说售价还算低廉，但也不是庶民能轻易买得起的。

克莱姆拿著的怀表是他借来的，因此与普通魔法道具时钟不同，具有特殊魔法力量。

时钟名称为「十二种魔法力量（Twelve Magical Power）」，每天一次到了订好的时刻，就会發挥该时间对应的魔法力量。

只不过想享受这种恩惠，至少必须拥有怀表一天，因此才刚借用的克莱姆无法發动魔法之力。

「嗯，时间到了？好快喔。」

身旁漫不经心地望著蓝天的女性对他说。

「好像是的。」

克莱姆回答这位女性──精钢级冒险者小队「苍蔷薇」的成员缇娜。

「是喔──像这样悠悠哉哉的，会搞不清楚时间经过呢。」

这句话有一堆地方可以吐槽。

首先缇娜并没有悠悠哉哉的，她是在这个地点──克莱姆身後建筑物的正面入口当警卫。而且她虽然说什麼「时间到了」、「好快喔」，但她应该拥有相当準确的生理时钟。

冒险者当中有些人拥有异常準确的生理时钟，尤其是盗贼系的职业特别多，这是拜训练所赐。因为他们负责暗中进行调查，常常需要单独行动，时间感非常重要。

「嗯，你有话想说吗？」

「不，没有。」

听了他的回答，「这样啊──」缇娜再度仰望天空。

克莱姆不可能特地问她为什麼要说谎，揭穿她隐瞒的事。

他本来是没钱雇用缇娜等人的，但她们说目的地正好一样，克莱姆是利用人家的好意，不能做出让人家不高兴的言行。

「那麼我去跟公主说一声。」

「慢走──」

克莱姆转过身去，走向至今背对著保护的建筑物。

工程中克莱姆看过幾次，不过今天是他第一次进入完工的建筑物内。克莱姆感觉到这栋建筑物的大小──其中蕴藏的自己主人的心意，心中一阵暖意。

打开门，一股可以形容成新屋气味的独特木头香，搔弄著克莱姆的鼻子。

他继续往前走，穿过通道，打开最深处的房门。

自己的主人就在那裡。

艳光照人的公主拉娜。

而她的周围有好幾个小孩。

她对吵闹的孩子们投以温柔微笑，倾听童言童语的模样正有如圣女。

面对这有如一幅画的光景，克莱姆说不出话来。

他怕自己会破坏了这副神圣不可侵犯的光景。站在窗边，雇用来在这设施工作的幾位女性似乎也是同样心情，谁都没动一下。

不过，这个房间裡只有一个人似乎不这麼想。

「喂，小子来喽，时间到了。」

面具下传来的冰冷声音，让拉娜抬起头来，正眼看著克莱姆。

克莱姆确定那双蓝宝石般的眼眸中，映照出了自己的身影。

「……非常抱歉，拉娜大人，回王宫的时间到了。」

「这样啊──那麼虽然捨不得，但我该走了。」

「咦──」孩子们依依不舍地叫了起来。若不是完全掌握了孩子们的心，是绝不可能让他们發出这种声音的。

孩子们的反应让幾位女性慌张起来，这才有了动作。她们安抚孩子们，实在不听话的孩子就硬是抱离拉娜身边。

「我还能再来找大家玩吗？」

对於拉娜的询问，孩子们一齐精神饱满地答应。

「那麼，下次我来做菜给大家吃喔──克莱姆，我们走吧，伊维尔哀小姐也一起。」

「哼，用不著你说，我是你的护卫──不对，我并没有接受委托，所以只是个同行者罢了。不用在意，我跟在你们後面。」

一行人一起走出建筑物时，停在附近的马车正好也到了。

缇娜没说一声就先坐进马车。看起来似乎很不懂礼貌，但她是为了确认安全。接著是拉娜，然後是克莱姆，最後伊维尔哀上了车，马车开始移动。

匡当匡当晃动的马车裡，伊维尔哀轻声说道：

「……不过，你也真辛苦啊，还盖那种孤兒院。」

「会很辛苦吗？」

「会啊，应该有很多人跟你说吧。说现在世局动荡不安，没钱干这种事。」

拉娜一根手指抵在下巴上，偏了偏头。

「不会啊，哥哥马上就答应了我的请求，而且正因为世局动荡不安，才更该保护孩子们。」

伊维尔哀扬扬下巴，要她说下去。

「是，就如你所知道的，魔导国的君王造成了大量死伤。我想会有很多孩子因此失去父母亲，所以为了保护这些孩子，我才成立了孤兒院，况且也需要给失去丈夫的女士们新的工作机会。」

「魔导王啊……这事之後再谈，但与其把钱花在死小孩身上，难道不该用在更重要的地方吗……我倒觉得弱者会死是没办法的喔。」

「话不能这麼说。」

拉娜斩钉截铁地断言，跟之前的说话方式不同，语气坚定。

「强者本来就该拯救弱者，再说……」

克莱姆感觉到拉娜稍微瞄了自己一眼。

（也许是──）

克莱姆的脑中浮现出兒时的自己。

公主是知道自己那时的模样，才会想到成立孤兒院吗？也为了不再增加更多克莱姆这样的人。

他胸口一瞬间发热。

当然，克莱姆并未确认拉娜的真实心意，但他觉得自己想得没错。

「好吧，这也是一种想法，再说我也不该把自己的观点强加在他人身上。可是，有必要盖那麼大一间吗？」

「是的，因为我预计会有很多孩子进来。而且我预定从皇家直辖地招募孩童，那样的设施还算小了。孩子们是我的宝物，我必须长期照料他们，以免他们走上歪路。」

「哦──公主殿下好聪明喔。」

「你想说什麼，缇娜？」

「伊维尔哀，你觉得失去父母亲的孩子能怎麼生活？」

「这个嘛……原来如此……国内人手不足，不能将珍贵的劳动力拿去填补士兵空缺，所以要用其他办法阻止治安恶化？……原来如此啊。」

「『有些人只要有人照顾就能活得清正廉洁，没人照顾时却可能输给慾望。而当他的犯罪行为成功时，就会越陷越深，小小的犯罪会像雪球般越滚越大。对这方面本来应该加紧预防，但是很难，所以要用这种方法减少漏洞。』」

「哼，『──不是每个人都一样坚强』是吧。」

「伊维尔哀也被说了啊──她是不是很喜欢这番话啊？」

「……同一番话我大概听那傢伙讲过三遍喔。」

後半是只有伊维尔哀与缇娜才懂的对话，不过前半部分讲了那麼多，克莱姆也听得懂。

失去双亲的孩子们，很多都会为了讨生活而染手犯罪行为。这麼一来本来已经削弱力量的八指恐将死灰复燃，王都治安也会变得更糟。

自己敬爱的君主是为了将来做準备，未雨绸缪吧。

然而──拉娜一脸不解地问伊维尔哀：

「──你们在说什麼？」

「喂……是我们想太多了吗，还是说她在演戏？」

「嗯──看起来是说真的。」

「既然你都这麼说了，那大概就是吧。总觉得白佩服她了。」

「呃，我的评价好像兀自大涨大跌的……可是呢，我也有仔细想过喔。这次创办的孤兒院如果进行顺利，给孩子们施予某种程度的教育，从中出现了优秀人才的话，其他贵族应该也会效仿。也是因为如此，我需要够多人数的孩童……这理由不太值得夸奖就是了。」

「不，如果是因为这样而募集死小孩的话我能理解，也觉得很佩服。若是将来能获得成果，那的确值得赞赏，再说不求回报的奉献只会让人起疑。」

「伊维尔哀因为吃过苦所以心态扭曲。」

「喂！你明明就跟我一样！」

「没那种事，我很纯洁，只有你心灵污秽。」

啧！面具底下传来好大一声咂嘴。

「对了对了，我之所以会成立孤兒院，是因为布莱恩先生给了我灵感。」

「布莱恩．安格劳斯啊。那傢伙在做什麼？今天没看到他。」

「布莱恩先生现在为了别件事在王都中奔波。」

「哦，有别的事比护卫公主更重要？」

「是的，他正在为了实现战士长的遗愿行动。呃，关於战士长那件事，那时给各位添麻烦了。」

缇娜稍稍眯细眼睛，以隐藏内心情感。

「竟然让我们魔鬼领队的漂亮脸蛋受伤，真气人。」

「真是万分抱歉，我代替父王向你致歉。」

「我知道你有直接向老大道歉，所以原谅你。」

「谢谢你。」

「……死者的话语有时比生者的话语更有力量呢。」

伊维尔哀的视线一时似乎拋向了马车小窗外，但只是短短一瞬间。

「回到正题吧，你说布莱恩．安格劳斯在做什麼？」

「战士长似乎说过希望布莱恩先生『继承战士长职位』，但他本人好像认为自己办不到。所以他说要找出适合继承战士长职位的人，由自己来锻炼。」

「如果由没有贵族门路的人来找……原来如此，葛杰夫与安格劳斯都是平民出身，所以才会有这种想法吧。而你从这裡获得灵感──」

「──是的，所以我才想到成立孤兒院。我在想，下次或许可以请布莱恩先生来见见孩子们。说不定在那些孩子当中，有人拥有才能。」

「我没看那麼多──」这是缇娜说的。「伊维尔哀呢？」

「光用看的看不出有没有魔法才能。如果做个幾次魔法训练，等那人能用魔法了，多少还能看得出来，但也只限魔力系魔法。就算那个死小孩拥有精神系或信仰系等才能，让我来看也看不出来。」

「嗯──」拉娜發出了烦恼的声音，然後展露花朵绽放般的笑容。

「将来我想请各方人士莅临孤兒院，让大家看看孩子们有没有才能。」

拉娜的视线朝向两人，她的视线比言词更能传达心意。

「……真是天真的想法，如果是那傢伙的话，啊──」

「很遗憾，伊维尔哀，如果是那个魔鬼领队的话──」

「──也是，但就算由她提出来，我也不会轻易点头喔。我要收取相应的报酬──既然受了雇用，最起码得收钱。每次都不收报酬，对其他人实在说不过去，也违反了冒险者的规定。再说我可是要传授技术，委托人当然得支付相应的代价。」

「我想你说得完全没错，我只能表示同意，但真的很抱歉，其实我没有钱……」

拉娜沮丧地说。

第三公主是备用品的备用品。拉娜唯一受到的期待就是嫁给贵族，为对方家族带来皇室血统，没有贵族想成为她的後盾，因此至今幾乎没有钱可供她自由花用。因为拉娜生活简朴，因此目前还没遇过问题；但换成第一公主或第二公主绝对受不了。

正因为如此，克莱姆更能感受到自己的铠甲中蕴藏了她的心意。

「我听说公主殿下都是穿著闪亮华丽的服饰，过著优雅生活耶──」

「现实情形没那麼简单的，不过，的确也有那样的公主就是了。」

真令人向往。看到拉娜两眼闪闪发亮地说，一种难以言喻的情感袭向克莱姆心头。

克莱姆希望这世界上最美丽，心地最善良的她能过著那种生活。

但另一方面，他觉得正因为拉娜是这样的人，自己才能获救，也才有现在的自己。就在这时，拉娜一转头，用散发美丽光辉的眼眸，与偷看她侧脸的克莱姆四目交接。

「──你在想什麼，克莱姆？」

「啊，不，没什麼，拉娜大人。」

「是吗，有什麼事要跟我说喔，有困难时必须互相帮助才行。」

「呃，是！谢谢大人！」

「喂，不好意思打扰你们谈情说爱，但我还是不喜欢免费教导技术。就算由那傢伙提出来，到时候我也要收取某种程度的费用喔。」

「届时还请提出我也能支付的金额。」

拉娜低头行个礼。

「嗯──不过公主想知道的是有没有才能吧？我来看身手，那伊维尔哀要做什麼？」

「……唔。唉，我就老实说了。只做幾次练习是看不透那个人的才能的。魔法才能著重的不是外在因素，而是内在因素。再说以魔法才能来说我是天才，但也仅止於此，没办法像帝国那个大魔法吟唱者一样使用能力。」

「以天生异能（Talent）看清才能啊──」

「天生异能啊……」拉娜唉地叹了口气：「这个也是，要是从小就看得出来就好了，这麼一来贵族轻视平民的僵硬思维应该也能减轻些。」

「这样的话只要建立体系，使用能看穿所有孩童的天生异能的魔法不就行了？如果只要是看清有无天生异能，第三位阶就有这种魔法。只不过若是要查出详细内容，似乎会是更高阶的魔法──哎，只能算梦话吧。」

「是这样吗，天生异能是能解读的吗？」

「我不知道你眼睛在发亮什麼，不过可别太期待喔。我只是听过要用到精神系的第三位阶魔法，才能勉强判断眼前对象有无能力。就算看出有能力了也很麻烦，还得查出怎样才能發挥能力。不只如此，调查了半天，很可能根本是个不怎麼样的能力。」

「这样啊……」

拉娜眼中的光辉消失了。

「比起这样做，还不如多方尝试。例如到瀑布下冲水，或是闻些不会太危险的药物让他出神。天生异能好像都是突然知道的，感觉就像什麼东西组合起来了。」

「是这样吗……唔，那时是这样吗？」

「哎呀，伊维尔哀小姐也具有天生异能吗？」

本来讲个不停的伊维尔哀，突然散發出岩石般的氛围，看来是讲到她不想被问到的话题了。

但克莱姆的主人却天真无邪地问道：

「可以告诉我你的是什麼能力吗？」

克莱姆有时也会觉得她意外地敏锐，但大多数时候她都是这个调调。或许可以说不会看场合吧，有时候能若无其事地问些不方便问的事。

她应该不是没顾虑到对方的心情，只是王族生活养成的习惯使然。

「你幹嘛啊，这麼让你感兴趣？」

「我身边没有幾位拥有天生异能的人士，所以很想知道伊维尔哀小姐拥有什麼样的天生异能──」

「是吗？既然如此，好吧，我就告诉你。」

伊维尔哀突然探出上半身，神情雀跃的拉娜也一样探出身子。

天生异能有时能够成为杀手锏，尤其是冒险者想必更是如此。克莱姆不觉得拉娜会大嘴巴说出去，但还是觉得这种事不该轻易告诉别人。

「这事我不想让别人听到，耳朵凑过来好吗？」

「好的。」

拉娜将自己的耳朵朝向伊维尔哀。

然後──

「这麼重要的事我怎麼可能随便说出去啊！」

怒吼声响遍了马车内。

身旁的缇娜似乎早就料到，已经用手指塞起了耳朵。

「好过分！耳朵都嗡嗡响了！」

拉娜就像扑进克莱姆的胸前般倒向他身上，要是加个声音形容，应该会是轻轻的「砰」一声吧。

拉娜眼角噙泪，从胸前抬头看向克莱姆。

好可爱，好香。克莱姆用力拋开自己的无聊想法。竟然对自己的主人怀抱这种邪念，真是太不像话了。

「伊维尔哀大人，我明白您的心情，但还是请您多多包涵──」

「──啊──？小子，都是你太宠她，这小丫头才会变成这样吧？」

「没……没有的事，我怎麼敢宠公主……」

就算想宠，他也办不到。

「就是啊！克莱姆可以再宠我一点的，我赞成伊维尔哀小姐的意见。」

「呃，不，公主，这样说似乎有点不对……」

「没那种事！只要你多宠我一点，这种时候被骂了，我也比较能坦率接受啊。所以请你多多宠我，总之先像小时候那样跟我一起睡午觉吧。来，伊维尔哀小姐，请再多讲他幾句！」

「够了，是我太笨了……总之小丫头，我不会把我的天生异能告诉别人，知道了吗？」

「真有那麼危险吗？」

「是啊，这是我的杀手锏。一旦使用了……对，就像我们领队的剑失控一样，拥有能轻易破坏一座都市的力量。」

伊维尔哀这番话很有份量。

但克莱姆的胸前传来了「嗯──？」的狐疑声音。他很想往下看看，但这样势必会强烈感受到拉娜依偎著自己，他把持不住。

就算想推开拉娜，她身子骨太柔嫩了，克莱姆不知道该使多少力道。

克莱姆的心臟正在怦怦狂跳时，话题仍在继续中。

「你是说拉裘丝那把剑吗？」

「对，据她的说法，那把剑一旦失控似乎会不可收拾。足以消灭一个都市……不，好像是国家？她说她为了压抑它，分出了不少力量。」

「原来是这样啊……我都不知道……」

克莱姆还没将魔剑的事告诉主人或任何人。

「你还是别放在心上比较好，魔鬼领队是不想让你担心才什麼都没说。希望你装做不知情。」

「……说得也是，我明白了，我会这麼做。」

「说到这个，艾因卓大人最近怎麼了？这阵子都没见到那位大人。」

「嗯，没人告诉你吗？喂，公主，你没告诉他吗？」

「……我忘了。呃，是这样的，克莱姆，她在陪格格兰女士与缇亚小姐修行。」

伊维尔哀接在拉娜後面说：

「那两人在与袭击王国的魔王亚达巴沃交战中丧命。当然復活是復活了，但復活之际失去了大量生命力。为了恢復生命力，她们现在正将自己置身于危险当中，以跨越生死关头的方式恢復力量。」

「其实我们本来也想去。」

「但我们去了会让她们内心深处产生依赖感。少数人战鬥才是短期变强的最佳手段。」

「这个说法也很值得怀疑。」

「唔──我听说这是有效率的『升级』手段……哎，总之也只能相信这种说法多多锻炼，不然如果那傢伙再度袭击王都，说不定连争取时间都办不到。」

「争取时间？啊──伊维尔哀，你是说替你最推的那个人？」

「没错！就是等那位大英雄到来！」

突然间，伊维尔哀的氛围变了。

从面具底下可清楚感受到她兴奋般的热情。

「记得是叫飞飞先──大人对吧。」

「正是！就是大英雄飞飞大人！双手持握巨剑，彷佛挥动树枝般运用自如的最强战士！不会错，那绝对是邻近诸国最厲害的战士！只要有那位大人在，就算亚达巴沃再度来袭，也一定能铲除敌人！上次很可惜让对手跑了，但照那位大人的个性，一定已经想好对策了！」

「呃，是。」克莱姆被她的热烈演说逼得只能应声附和。

「可是那个人这次能来吗，他不是成了那个魔导王的部下？」

看到伊维尔哀握紧双手，缇娜难得表情有些疲累地出声问道。

「啊──！飞飞大人！可恶的魔导王！竟然敢控制那位大人，就算上天允许，我伊维尔哀也绝不饶恕！要是能打倒那魔物，解放飞飞大人就好了！他是不是有什麼打算，我看我还是去耶．兰提尔一趟，听听飞飞大人的想法吧？」

「……等她们俩恢復力量再说。」

「我只是去一下，记住地点了我就用传送回来。单程利用『飞行』的话，也不会花太多时间！」

「伊维尔哀，你真的一讲到飞飞就会發狂……魔鬼领队不是说了不行吗？」

「只要你保密就好啦！」

「我其实口风很松的，膨松柔软。」

「从你的上个职业来想，怎麼想都不可能吧？」

「很遗憾，现在的我是冒险者『苍蔷薇』的缇娜。绰号是『大嘴巴』。」

这时缇娜的眼光变得严肃。

「……好机会，我想趁现在问你。伊维尔哀，你杀得了魔导王吗？」

伊维尔哀顿时僵住了，刚才那种兴奋情绪不復存在，在那裡的是冒险者最高阶的魔法吟唱者。

「如果传闻全数属实──那人已经超越了一个魔法吟唱者能拥有的力量。後来我也稍微查了一下卡兹平原發生的事，仰赖各种人脉──还联络了那老太婆分析情报，老实说实在太荒唐了，真希望只是小子中了幻术。」

「那绝不是幻术，死了那麼多人……」

拉娜的神情悲痛地扭曲。

「那场战争有二十六万人上战场，其中死者多达十八万人。此外我还听说有些人精神失常，变得无法过正常生活。来到那所孤兒院的孩子当中，也有人的父母亲是这样的。」

「……听到小子的说法，我只能这麼觉得。你们竟然被那样可怕的魔物追杀……」

「……是的，那完全是地狱。幸运的是我跟布莱恩先生还有……战士长这两位强者在一起，才能免於發疯；但直到现在，我有时候还是会忍不住回头看看背後。民兵的话恐惧感恐怕更强，就算發疯了也不奇怪。」

「你真的该感谢自己的幸运。」

克莱姆只点了一次头。

「那麼，缇娜，我诚实回答你的问题吧。我不可能战胜魔导王。」

这是早就料到的答案。

「果然。」

「是啊，如果只是那个召唤出来的怪物或许还有办法，但还是要实际看到才说得准。不过，能召唤好幾只那种怪物的魔导王，说实在的，不应该存在於这个世界，那是拥有神代力量之人。」

「有没有可能不是魔导王个人的力量，而是用了某种道具召唤？」

「是有可能，但不能妄下结论。不过也没有办法确认就是。」

「要是他能跟亚达巴沃狗咬狗就好了。」

「谁都希望能变成那样，再来如果能由飞飞大人杀了魔导王，那就更好了……」

「飞飞大人与魔导王，您认为哪个比较强呢？」

克莱姆虽然这样问，但他个人认为能召唤那样强大魔物的魔导王要比飞飞强多了。然而伊维尔哀却陷入沉思，让他相当惊讶。

「不知道，我个人希望击退了亚达巴沃的飞飞大人比较强，但魔导王的力量也超乎想像。两者的力量都与我们相差太远，连想像都想像不来。」

「那样的人物成了魔导王的部下，真是糟透了，谁都不敢惹他们。」

正是如此。

唯一或许能跟魔导王抗衡的人物成了魔导王的部下，情况令人苦恼。若是向魔导王挑起战端，就等於要对抗两个魔导王。

就在马车内的气氛变得有些阴沉时，有人敲了敲车夫座隔板上的小窗，将它打开。

「即将抵达王宫。」

听到车夫的声音，拉娜慢慢撑起身子，然後轮流看看坐在前面的两名冒险者。

「今天真的很谢谢你们，等拉裘丝回来了，我想跟你们吃个饭兼表示谢意，可以帮我转达她吗？」

●

闻报妹妹回来了，第二王子──赛纳克．瓦尔雷欧．伊格纳．莱儿．凡瑟芙离开房间前去迎接。

兄长──巴布罗．安德瑞恩．耶路德．莱儿．凡瑟芙第一王子下落不明已经过了一段时日，被认为不可能生还，因此他幾乎已内定为继任国王，这样的他主动前去迎接妹妹，本来是不合理的。因为即使是兄妹，也有著明确的身分差距。

即使如此他仍然亲自前往，是因为有事想紧急讨论。失去了优秀左右手的他，虽然不怎麼喜欢这个妹妹，但也只有她能靠了。

不久，赛纳克看到了妹妹的身影。

妹妹身边还有身穿纯白铠甲的克莱姆侍立。拉娜不管去哪裡，克莱姆大多都会随侍左右，因此这景象并不奇怪。

拉娜捡来的贫民小孩克莱姆。

以前他认为拉娜是耍花痴一时兴起才会捡个小孩回来，然而自从他知道了拉娜的怪异与无比智慧後，就开始觉得她这样做或许有其理由。

而这点从亚达巴沃袭击王都之时，以及魔导王大屠杀的结果当中，渐渐变得明显。

在这王都当中，很少有士兵比克莱姆强。就连葛杰夫挑选的战士团当中，与克莱姆同等或在他之上的人都寥寥可数。

再加上拉娜还与克莱姆带来的布莱恩．安格劳斯以及精钢级冒险者小队「苍蔷薇」领队拉裘丝建立了个人的友好关係。自己的妹妹在王都内是拥有最大物理力量的存在，这是无庸置疑的事实。

──她该不会是想用武力推翻政权吧？

赛纳克会这样怀疑也是理所当然。

就算拉娜不会这麼容易使出直接手段，也得多加戒备。因此赛纳克为了以防万一，正大费周章跟山铜级冒险者与秘银级冒险者秘密建立个人联繫。

赛纳克对自己的王子长兄表达感谢之念。

是因为长兄失踪，自己幾乎确定成为王储，才能够这样想方设法。长兄的年薪转给了自己也是一大因素。

话虽如此，巴布罗第一王子的遗体尚未發现，也的确让赛纳克感到些许不安。要是被魔导国俘虏了会很棘手，如果是受伤躲在哪个村子，那也很伤脑筋。

「真的……到最後都给我找麻烦。」

他在口中喃喃自语，不让侍从听见。

直到获得更稳固一点的地位之前，应该避免刺激贵族们。

目前的赛纳克在後盾方面有所不安。

约好一同促进王国發展的雷文侯爵拒绝了赛纳克的劝留，回自己领地去了。他有很多领民丧命，这样做是理所当然的，但他却给人一种永远不会再回来的感觉。

骄傲地谈起的平民出身的军师，以及珍藏的前山铜级冒险者死亡等等，肯定也成了原因之一。

赛纳克感到自己的胃附近产生一阵轻微痛楚。如果找妹妹谈谈，这阵痛楚是否能舒缓一点？

赛纳克这几周以来，怀抱著一个问题。

那就是该不该赠送贡品给魔导王；如果要送，是要以建国纪念为名义，还是用别的理由赠送。

目前比较妥当的选择应该是不送，送礼给夺走我国领土建国的国家，就算被邻近诸国理解成从属的证明也怪不得人。然而与魔导国加深友好关係，却是非常重要的一件事。

虽然魔导国的战力依然不明，但光凭魔导王一个人就足以毁灭国家，这是已知的事实。

无论如何都得避免他的目光继续朝向王国。

正因为如此，赛纳克才会想到赠礼──他个人觉得被理解成从属关係也无所谓──以尽量争取时间。

但麻烦的是贵族们不可能同意。

没错，很多人都知道了魔导王的力量。但他们恐怕不会允许继任国王（赛纳克）对这种强者表现出臣服态度。

贵族们蒙受巨大损失，正在寻找能够發泄不满的牺牲品。

心腹葛杰夫．史托罗诺夫战死沙场，使得现任国王（兰布沙三世）因悲叹与动摇而心慌意乱。看到国王出丑的贵族们似乎是消了点气，但对於大败的国王，以及更进一步对王室的怨恨可不会因此消失。

（如果是那傢伙的话，应该能想到更好一点的主意吧。）

如果可以，赛纳克很希望能自己想出答案，但时间已经拖得太久，差不多该做出结论了。

赛纳克停下脚步，并大声踏响鞋子。

拉娜对这声音做出反应，脸转向自己这边。然後她转换方向，往赛纳克这边走来，这样上位人士的面子就保住了。

不久妹妹站到了他面前，但他不主动说什麼。目前是敏感时期，得继续让许多人明确知道谁才是王。

「我回来了，哥哥。」

「你回来了，妹妹。」

妹妹保持著公主的仪态行礼，赛纳克高傲地回答。他眼角餘光看到克莱姆也行了一礼，不过他不会对区区一介士兵答礼。

「一起走到半路吧。」

「乐意之至，哥哥。」

赛纳克带著拉娜迈出脚步，他下巴一比，要侍从离远点。一看，拉娜也用动作要克莱姆离远一点。

「那麼哥哥，您似乎有急事，究竟是怎麼了呢？」

拉娜仍旧面带笑容，小声向他问道。

「是不是魔导国的使者莅临了？」

赛纳克感觉心臟重重跳了一下，他只顾著想自己的行动，完全没想到对方会对自己说出这种话来。

拉娜大概是判断对方差不多该开始行动了吧。

赛纳克将此事记在心中的记事本裡，摇摇头。

「不是。」

「其他还有什麼事能让哥哥特地来见我呢？」

「嗯，我在想该如何赠礼。」

「我想使者莅临时，只要赠送哥哥目前所想礼品的一倍就行了。一半是慰劳使者远道前来，剩下一半是──不用特地说了吧。」

赛纳克一语不发，细细玩味拉娜所言。

这招真是太妙了。

只要说这是对使者特地前来的赏赐，贵族们也不能说什麼，就算他们内心知道有别的含意也一样。

看到拉娜立即想出办法解决自己想破头的问题，赛纳克再度感受到她的可怕。然而武力方面是拉娜的部下比较强，就算他想用手段杀掉拉娜，也只会反遭击退。既然如此，就只能选择怀柔。

「……等我当上国王，我会选个穷乡僻壤给你做领地，你就去那裡吧。」

「是，我愿听从哥哥所言。」

「一旦我将你送出去，就不会再把你唤回王都了。或许会有点不自由，但我预计选个生活不愁吃穿的领地给你，你就在那裡终老一生吧。」

「是，谢谢哥哥。」

不用再多说拉娜应该也懂，但赛纳克还是说出口，以卖个人情。

「你可以在那裡收养个无父无母的孩子当自己的小孩，任凭你高兴。」

「谢谢您，哥哥。」

拉娜回得很快，大概是她也在想这件事吧。

赛纳克不明白妹妹为什麼这麼爱克莱姆这个平民，相貌平平又一无所有，怎麼想都配不上妹妹。

（喔，对了，那时候听过这小妮子讲过自己的性癖好。）

赛纳克想起了很想遗忘的妹妹的可耻之处，有点可怜起克莱姆来。

「我会期待哥哥登上王位，等您成了国王，若您偶尔还能想起我住在乡下，我会很高兴的。」

「嗯，我会的，妹妹。不过，如果你偶尔还能给我出点主意，我也会很高兴──唔？」

赛纳克看到有个士兵小跑步往这边跑来。

那个士兵是葛杰夫战士团的一名倖存者。

他在那个战场上对国王尽心尽力，因此即使如今失去了战士长，他的地位仍然安泰，并受到国王深度信赖。顺便一提，国王也同样信赖拉娜的两名部下。

赛纳克想起了自己父亲枯槁的面容。

「王子，陛下召唤。」士兵喘口气後看向拉娜：「也请公主一同移驾。」

「什麼事？」

「是，据报魔导国的外交使节团即将前来。」

赛纳克视线只看了拉娜的侧脸一眼，回答：

「知道了，告诉父王我马上过去。妹妹，我先走一步，你準备好再过来。」

「我明白了，哥哥。」

直到刚才都待在孤兒院的拉娜穿著不显眼而简朴，这样会在贵族面前丢脸。

说完，赛纳克就露出险峻神色，比拉娜先迈出脚步。

「……呵呵，这提议已经完全不吸引我了，您提得太晚了。」

2

报告说魔导国的使节团将会花一星期从耶．兰提尔来到王都。

而今天就是第七天，若是按照预定，使节团就会在这天抵达。

赛纳克穿著穿不惯的铠甲，与左右列队的骑士们一起在王都往耶．兰提尔方向的门前整队。

持续数天的阴天难以相信地变得晴朗，春天舒爽的天空辽阔无际。

然而远处仍然笼罩著厚厚云层，只有王都上空才有蓝天。

情况实在太异常了，实际上，王城裡的气象观测师也扯著头髮说「不可能」。

他从很久以前就在王宫服务，如果是预测隔天的天气，准确率随便估计也超过九成。既然他都这麼说了，这片蓝天应该不是自然现象。

赛纳克在头盔底下呼出一口气。

他没听老师说过操控天候的魔法，但最好认为这对那个魔导王来说轻而易举。

赛纳克没有一个部下对魔法或各种异质力量拥有足够知识，这让他伤透脑筋。更正确来说，他以前太依赖雷文侯爵了。

雷文侯爵从冒险者身上取得知识，并汇整编纂成参考书。内容包括他们所知道的魔法道具的种类与形状、魔物的种类与能力，以及魔法的种类等等。

至今这些知识，身为同盟者的赛纳克也能拿来运用。然而如今雷文侯爵不在王都，参考书也没了。

赛纳克在其他贵族当中寻找像雷文侯爵一样从冒险者身上获得知识的人，但很遗憾地没有这种人。并非因为其他贵族愚蠢，而是冒险者的生活对贵族们来说简直是不同的世界。有贵族会试著拉拢冒险者，但那只是想依赖他们的力量，而不是想了解他们的社会与知识。

在王国两百年的歷史当中，贵族一直是如此。就这层意义来说，雷文侯爵才是特异分子。

（引退的冒险者──而且还要秘银以上的人，真有这麼容易找到吗？）

他曾听说冒险者大多厌恶政治相关的麻烦事，的确，政治领域与自由相差甚远。这样厌恶政治的冒险者，会在引退後来到自己身边吗？

赛纳克不禁心情黯然。

「──王子。」

身旁骑士的声音让他回过神来，往道路前方一看──就在那裡。

看起来像个黑点，但的确是魔导国的使节团。

赛纳克已经施加压力，禁止今天任何人通过这条道路。也没有人从背後大门来到这条道路，只有今天大门是关闭的。

「好，我再确认一遍。对方等同於国外贵宾，任何人企图对魔导国的使节团做出任何行为都是重罪，立刻处以死刑。」

「是！」

排成一列的骑士们气势十足地回答，一齐發出拍打腰际佩剑的声响。

「好！那就尽最大礼节，来场競争双方国威的战争吧！」

「是！」

一行人维持不动姿势，直到使节团抵达。

不久，使节团的前导先抵达了。

这是个黑铠骑士，骑在红眼熠熠的漆黑独角兽上。然而铠甲底下恐怕并非人类，散發出的气息有如缕缕热气般摇曳，让人感受到生命危险。穿著的全身铠（Full Plate Mail）更像有生命般脉动著。

赛纳克感觉到自己底下的战马抖动了一下。

对方如鸟爪般的金属手套放开缰绳，啪一声拍打胸前。

「恕我骑著马發言！我们是安兹．乌尔．恭魔导国的使节团！」

若要比喻的话，那声音就像快要腐朽的弦乐器般刺耳。光听就让人害怕，受到不安所苦。赛纳克为了挥开恐惧，高声说道：

「我乃里．耶斯提杰王国第二王子赛纳克．瓦尔雷欧．伊格纳．莱儿．凡瑟芙！陛下已命我等带领各位前往王宫，请各位随我等来！」

「了解，那就听从各位的带领吧。我的名字是──请见谅，由於我没有名字，因此请容我以种族名回答，我乃死亡骑兵（Death Cavalier）！」

听到种族名三个字让赛纳克瞠目结舌，但不能迟了回答。

「可以呼唤你为骑兵阁下吗？」

「再好不过了。」

「知道了，那麼首先，能否让我在此向使节团团长阁下致意？我是第二王子，负责团长阁下在王宫内的行动，希望能现在就让阁下记住我。」

「了解，我这就去请示团长阁下。」

「感激不尽。」

前导掉头回去。

虽然已经有一堆地方让人想吐槽，但对方可是那个魔导国。对於支配不死者、役使魔物的国家，最好别以为一般常识还能通用，期待使节团团长能长得像人类一点恐怕也太傻了。

「好了，绷紧神经，千万不能失礼了。」

「是！」

听了骑士们的回答，赛纳克将力气灌注进丹田。

使节团往王都而来的途中经过了幾个城市，因此他很清楚使节团的阵容。

马车数量有五辆。

每一辆都是由马型的可憎魔物拖拉，而护卫周围的也是魔物。有很多死亡骑兵，但也有其他形状的魔物。

他不知道这些魔物叫什麼名字，有多危险。但不管知不知道，己方要做的事都不会改变。来者是那个魔导王派出的使节团，绝不能有所失礼。

使节团靠近过来，从中走来一名死亡骑兵──应该就是刚才那个人。

「久等了，团长阁下──安兹．乌尔．恭魔导王的左右手雅儿贝德大人表示愿意见您。赛纳克阁下，请走这边。」

赛纳克做个手势要周围的骑士们留在原地待命，然後迈出脚步。

老实说他很害怕。

因为他要走在至今从未看过的魔物当中。

即使如此，他仍然有身为皇室成员的志气。赛纳克想必不久就会成为国王，今後势必还会多次与魔导国使者相见，不能在对方面前丢脸。自己反而必须趁此机会好好表现，让对方回去告诉魔导王裡．耶斯提杰有著傑出人物。

赛纳克一边觉得渗出的汗水很不舒服，一边下马，站在马车前。

「那麼，这位就是使节团团长，雅儿贝德大人。」

他振作起精神，不管出现多令人作呕的怪物，都不能改变表情。

车门打开，一个身影慢慢下了马车。

那是个──好美的女人。

不，赛纳克是没有更好的形容词，只能说她是个绝世美女。

不可能有人的美貌能与拉娜相比。赛纳克一直是这麼想的，但看来他错了。两人的差别，可以说相较於拉娜的月貌花容，雅儿贝德属於阴柔的妖艳美女。

雅儿贝德踏上了马车的台阶，鞋跟發出的轻微声响将赛纳克拉回现实。

赛纳克立刻下跪低头。

虽说是外国使者，但贵为本国王子之人下跪或许有点难看。然而考虑到王国与魔导国的国力差距，这种行为是正确的。现在王国需要的不是什麼骄傲。

是实际利益。

「可以请您抬起头来吗？」

头上传来静谧优美的嗓音。

「是！」

抬头一看，美女正漾著沉静笑容，低头看著赛纳克。

那是惯於管理部属之人的态度──不，她是人吗？

赛纳克目光不动，悄悄观察她。首先，长在腰上的羽翼是否为魔法道具？还有侧头部的犄角状物体也是。

不管是魔法道具还是异种族，想到魔导国的国家性质，两者都有可能。

「我叫雅儿贝德，以安兹．乌尔．恭魔导国的使者身分前来。虽然只有短短幾天，但仍请您多多关照──好了，请站起来吧，王子殿下，别这样一直跪著。」

「感谢您。」

赛纳克一边站起来，一边却想「这下问题来了」。

即使面对面交谈，对方仍然只说出雅儿贝德这个名字，大概表示她只有这个名字吧。

在王国──帝国也是──平民会有两个名字，如果是贵族就是三个──加上称号是四个。王族是四个──加上称号是五个。

所以他们才会嘲笑只有四个名字的吉克尼夫．伦．法洛德．艾尔．尼克斯不是王族，但听到雅儿贝德这个简直像假名或通称的名字，贵族们会不会对她做出蠢事？

他很希望自己是杞人忧天，但又不敢断定。

这是因为很多贵族捐躯沙场，由於当家或是一家老小死伤惨重，很多家族都是让备用品的备用品继承家业。

称之为备用品的备用品，也就是说当上贵族的尽是些不太受期待的人，既没格调也无知识，因为没受过那样的教育。

本来应该由隶属派系的上级人士教育这些人，但同样是因为上一场战争，使得大家没有这个餘裕。结果造成无能之辈被扔著不管，无能与无能集合起来，无能派系于焉诞生。

目前王国贵族的格调一口气低落，在这种状况下，他们能从容有礼地接纳名叫雅儿贝德的女性吗？

「……恕我失礼，该如何称呼雅儿贝德大人呢？」

这问题问得有点勉强。

其实他是想问：「雅儿贝德大人拥有何种爵位，或是在魔导国位居何种地位？」但是这样说，人家搞不好会说「你们这个国家怎麼连邻国使者的地位都不知道」。

不过，这得怪魔导国。

这是因为魔导国完全不肯泄漏国内有什麼样的人。建国已经幾个月了，幾乎都只是内部作业，这恐怕是他们的首次主动外交。

关于雅儿贝德，赛纳克只有刚才听说她是使节团的团长，也是魔导王的左右手，如此而已。

（帝国八成知道些什麼……但没有传来任何情报……也是，都能要求对方使用那种魔法了，大概真的很憎恨我国吧。）

雅儿贝德似乎看穿了他的迷惘，回答道：

「本人猥当大任，获赐安兹．乌尔．恭魔导国的楼层守护者、领域守护者以及全体总管的地位。」

「哦，原来如此。」

赛纳克虽说著原来如此，却不知道总管指的是什麼，应该说他不懂楼层是什麼意思。对方似乎看出了他隐藏的困惑，接著说道：

「这麼说吧，我有幸担任安兹大人──不，担任恭魔导王陛下的次席，守护者总管的地位。」

「哦，原来是这样啊！」

（听说她与魔导王的关係亲密到能称其为安兹大人，看来是事实了。侯爵……不，是公爵吗？这件事得尽早回去向众人说明。不过她说守护者……总管？）

「那麼雅儿贝德大人，我先带您前往王宫。我想将位於王城一隅的贵宾馆做为各位逗留王都时的宿舍。父王──兰布沙三世年事已高，只能在王城入口迎接各位，还请见谅。」

「没关係。」

笑容完全没有失色。

若是以一般关係来说，她应该要对王子表达谢意。她这种态度明确传达了彼此的上下关係。

赛纳克的背部冒著汗，因为他明白到要与对方建立友好关係很难。

「……此外，本来我国应该鸣钟祝贺，但由於我国与贵国想法上的不幸差异造成了悲剧，使我方无法鸣钟，还请见谅。此外我们并未告知民众各位莅临，这点也希望您原谅。」

「当然没关係。」

要是知道魔导国的使者来了，不知道民众会做出什麼事来，因此他很感谢雅儿贝德如此回答。

（我应该当作欠了对方一个人情吧……）

对方看起来一点都不像是怕暴徒袭击使者团，不只是刚才那个死亡骑兵，在场的人物恐怕都是魔导国首屈一指的强者。就算他们说每个人都能与葛杰夫．史托罗诺夫匹敌，他也能相信。

「那麼我也可以问些问题吗？」

「是！只要是我能回答的，请尽管问。」

「首先可以告诉我到了王宫之後的预定行程吗？」

「是！首先，今晚预定请您与我等王族进行宫廷晚宴。接著明天是舞台鉴赏会，明晚是召集王国贵族举办的自助式宴会。後天是宫廷乐团的音乐会──之後我们安排了外交交涉的时间。」

「原来如此……可以再安排个参观王都的行程吗？」

「当然可以，我们精挑细选了一批骑士保护您的安全。」是护卫，但也是监视与屏障。「您有哪些感兴趣的地点吗？」

当天必须完全封锁该地点，不让平民靠近。

「不……没有特别哪个地点。我不知道王国有哪些名胜，如果可以，希望能请各位担任向导。」

「好的，那麼我会如此安排。」

雅儿贝德面带笑容点头。

3

菲利浦在这一个多月里，一直觉得自己是王国前幾名的幸运儿。

他个人觉得搞不好是最幸运的，不过谦虚才是美德。况且说不定有哪个贵族比自己更幸运，所以还是收敛点才是聪明人。

（贵族──是吧。）

菲利浦抿紧差点笑出来的嘴角，拉整衣服的皱褶。

他是第二次参加贵族们的这种聚会，不过不愧是皇室主办，铺张程度绝非上次参加的宴会能比。

参加者身上的服饰也比上次更精緻耀眼，不知道一件衣服花了多少钱。

菲利浦看看自己不起眼的服装，感到有点烦躁。

高阶贵族们的穿著就是精美。

身穿华美服饰的贵妇们正在微笑，但那会不会是在嘲笑自己的穷酸打扮？他忍不住毫无根據地这样想。偷看一下周围，就觉得在场所有贵族好像都在取笑自己。

（都是没钱害的。）

要是领地有钱，就能穿更好的衣服了。然而菲利浦的领地并不怎麼富庶，现在穿的这件衣服也是紧急拿哥哥穿过的修改而成，因此肩膀部分有点紧。

（没钱是因为至今为止的每个统治者都昏庸无能，所以我这个次任统治者可要做得有声有色。）

菲利浦生於贵族家庭，是三少爷。

就跟平民一样，即使在贵族家庭裡，三男也不怎麼受欢迎。不管是多富裕的家族，过度分割财产都会失去力量。所以无论平民或贵族，基本上都会将全财产给长男继承。

如果是富裕的贵族家庭，三男或许也能得到金钱支援等等获得自立，有门路的贵族家庭说不定会将三男送出去当养子；但菲利浦家并非如此。

当长男迎接成年之时──病死的可能性减少时──三男就幾乎成了无用的存在。

要不就是拿到一点钱被赶出家门，要不就是得到一栋破房子，像个农夫一样幹活。菲利浦的人生应该只有这两条路，但事情并没有变成这样，他还得以在如此华丽的社交场合初次亮相。

所以才说菲利浦是幸运的。

第一个幸运是次男──他的二哥在成年前患病而死。

本来在长兄──长男──成年时，次男就已经没多少价值，再加上领地贫困，没钱请神官。因此家裡以药草为次男治疗，但始终不见好转，就这样过世了。

如此菲利浦的立场就提升到了备用品，价值从农夫上升到管家。

这还不算稀奇。

菲利浦之所以觉得自己在王国是前幾名幸运儿，是因为後来發生了更幸运的事。

自己成年过了幾年後，就在兄长即将继承父亲的领地时，王国与帝国之间爆發了那场战争。若是按照往年惯例，应该只会以互相示威告终。就某种意义来说，这是一场正好让兄长累积功勋的安全战争，他也是为了这个目的而出兵。

然而，兄长没有回来。

他被魔导王的魔法波及，与同行的二十名农夫一同捐躯。

菲利浦忘不了自己接到这份死讯时的喜悦，不再是备用品的喜悦。

只是，遗体没送回来，代代相传的全身铠没回到家裡一事，让菲利浦有点不高兴。不过冷静一想，就觉得这没什麼大不了的，只要用领地的税金做一件更好的铠甲就行了。比起这种事，能够继承原本遥不可及的领主地位更让他高兴。

时机也刚刚好。

如果兄长是在继承家业後才死，菲利浦这个当家顶多只能当到兄长的孩子长大。多谢他还没继承就死了，自己才能确定成为领主。

好像魔导王是为了菲利浦而这麼做的一样。

为此，菲利浦甚至对从未谋面的魔导王产生了亲近感，如果可以，真想把这份感谢传达给魔导国使者。

不──

（对，我应该更进一步利用这份幸运。我正在走运，怎麼能错失这个机会！）

菲利浦心中的怨气熊熊燃烧。

他至今看著父亲与兄长的所作所为，觉得他们真是蠢到家了。他总是想：为什麼不这样做？这样做明明可以获得更多利润。只是他从没开口跟兄长们说过。

因为他知道就算说了，从中产生的利润也不会分给自己，让领地富庶的名誉也是。所以长久以来，他一直把经营领地的点子藏在心裡。

（我要让近邻的领主们知道我才适合当领主，让大家知道父亲瞎了眼才会让兄长当继承人。我要把高品质的麦子与蔬菜强卖给众多商人──不对，该怎麼做？要是因为这样而引人注目，我想出的划时代创意被盗用怎麼办？可是不卖就赚不了钱，我得找口风紧，值得信赖的商人──那傢伙不行。）

菲利浦想起御用商人那个男人的嘴脸，表情变得扭曲。

尽管在这样华丽灿烂的会场，自己原本心情兴奋，但想起御用商人的嘴脸，不愉快却胜过了兴奋。

（竟敢瞧不起我！现在我就忍忍，但我一定要在王都找到优秀商人，撵走那傢伙！我已经有门路了！）

菲利浦称讚来王都才几星期，却已经渐渐建立人脉的自己，赶走不愉快的感受。

（真不愧是我，已经建立了这麼大的人脉。我一定要让我的领地富庶起来，赚到大把金币。我要让瞧不起我的所有蠢蛋知道，他们是把谁当蠢蛋！）

菲利浦正在想像指日可待的光辉未来时，会场响起了男人的声音：

「各位来宾！现在为各位介绍魔导国使节团团长雅儿贝德大人！」

演奏著沉静曲调的乐团停止奏乐，本来在谈笑的人们安静下来。

一看，礼官站在门扉旁，似乎叫到了这次皇室主办自助式宴会的主宾名字。

「雅儿贝德大人在魔导国被称为魔导王陛下的左右手，身任相当于宰相职位的守护者总管地位，本次是独自入场。」

菲利浦附近传来女性小声说：「哎呀，一个人吗？」站在身旁看似富裕的贵族规劝著她：「不可无礼。」菲利浦露出有点不可思议的表情。

（一个人也不会怎样吧？话说回来，竟由地位如此崇高之人担任来使！魔导国对王国这麼有兴趣？）

菲利浦想看看来者是个什麼样的男人，眼睛看向礼官身旁的门扉。

「那麼欢迎使节团团长雅儿贝德大人入场！」

门扉打开，室内悄然无声。

在那裡的是个美若天仙的女子，姣好容颜比菲利浦至今见过的农民、来到王都後前往的妓院的女子都要美。刚才见到的公主的确也很美，但菲利浦比较喜欢这一型的。

服装也无可挑剔，白银礼服配上金色发饰，礼服的下半部覆盖著黑色羽翼般的物体。反射著自上方飘落的魔法灯光，看起来就像她自己散发光彩。

菲利浦侧眼看看刚才说话的女人，她像是愣住了，露出一副白痴相。

（怎麼，怎麼？连伟大贵族的女伴都露出这种表情啊，简直跟四处可见的农民没两样。）

菲利浦心中涌起了胜利情感，他本来就对魔导国有些亲近感，来自该国的使者赢了，引起了他心中的喜悦。

「欢迎，雅儿贝德阁下。」

兰布沙三世从座位站起来，迎接雅儿贝德。

「陛下，感谢您的邀请。」

看著雅儿贝德侧脸的菲利浦，看到她脸上笑容满面。

（真是难以言喻的美啊……）

「抱歉我年纪老迈，必须使用椅子。好了，王国的各位贵族。主宾已经莅临，今晚你们不用拘束。来，也请雅儿贝德阁下尽情享受。」

「谢谢陛下。」

雅儿贝德微笑著。

菲利浦瞄了一眼刚才那个女贵族，她正在说著「她没低头」等莫名其妙的话。菲利浦忘掉笨女人的胡言乱语，目光追著绝世美女。

她与拉娜公主亲密说话的模样，让人想烙印在眼底。

（要是能把那种女人据为己有，那就太棒了……）

即使是菲利浦也知道这是很难的，但也觉得不无可能。

（只要让领地富庶，自然会有贵族家族想把自己的女兒嫁给我。要是更富庶，当然会有更好的女人来。那个公主或那个使者，也不是一定不可能。）

菲利浦感觉到一股热情从下腹部涌升而上。

（高高在上的贵族似乎还会纳妾……要是能同时享用那麼漂亮的两个女人，那可是棒透了。）

雅儿贝德与拉娜。菲利浦交互看看两人。

淫猥幻想差点就要膨胀起来，菲利浦急忙去拿饮料，再怎麼说也不能在这种场合鼓著裤裆。饮料滑落喉咙的冰凉感，使他恢復冷静。

（不过这冰块是怎麼做的？应该是魔法吧，但是……）

在菲利浦的领地，顶多只有神官会用魔法。他们能治病疗伤，但是会索取费用。如果要制作冰块，应该会索取同等的金钱吧。

（既然待在我的领地，我应该叫他们免费为我治病疗伤。区区领民还敢跟领主收钱，岂不是很奇怪吗！）

菲利浦在心中记下对神官的这项要求，做为一项新政策。

等回到领地後要从哪裡著手？真令人期待。每一项好主意都将为自己带来黄金的光辉。

（──哦？）

将视线转回雅儿贝德那边，就看到她一个人站著。

周围虽然有一些贵族，但看起来就像不知该如何攀谈。

（魔导国啊……王国今後会怎麼样？）

他才不管王国以後会怎样，但自己的领地若是出问题就伤脑筋了。

既然如此──

菲利浦为自己的想法背脊一震。

（──喂喂，不要有这麼危险的想法啦。可是……这招似乎还不错……吗？真是太强了……竟然能想到这麼厲害的点子……）

他看得见雅儿贝德有些寂寞的侧脸。

（第三名不行，第二名也没意义，就是要第一名才有意义。）

魔导国使者看起来像是因为没人找自己讲话而无地容身，菲利浦从书上知道女人最怕这种状态。

（我得踏出一步，要下赌注才有报酬。要让状况产生变化，才有往上爬的机会。我是幸运儿，应该善加运用这份幸运。）

菲利浦家从很久以前就隶属於一个派系，但顺序从下面数比较快，他不觉得隶属於那个派系有受到什麼恩惠。

菲利浦想起最近人家对他说过的话，某个削瘦的女主人说过：「不如由您發起一个新派系吧？」

菲利浦下定决心，将原本喝不完拿在手上的酒杯一仰而尽。

这跟家裡喝的那种掺水淡酒不同，烧灼著喉咙与胃。如同受到腹部涌升的热量推动，菲利浦踏出脚步。

「雅儿贝德大人，方便打扰一下吗？」

菲利浦上前搭话，雅儿贝德对他露出了笑容。

他的脸红绝非起因自酒力。

「哎呀，初次见面──」

她皱起眉头想了想，菲利浦马上明白到她要的是什麼。

「我叫菲利浦。」

「咦？啊，菲利浦卿──不对，大人，很荣幸能见到您。」

「我才是，能见到雅儿贝德大人，是我无上的喜悦。」

菲利浦能感觉到周围的气氛起了些许变化。

视线稍微移动一下，看到就连远远旁观的高阶贵族们都一脸惊讶。

在这王室主办的自助式宴会裡，此时自己受到所有人的瞩目，这种实际感受真是愉悦的极致。

（我……我现在，正站在众人的中心！）

坐冷板凳的自己，如今受到王国贵族──王国大人物们的注目。这麼一想，难以置信的兴奋支配了菲利浦。

（没错！我就是菲利浦！看清楚了！看清楚今後将站在王国中心的人是什麼模样！）

菲利浦拚命动脑，做出了一辈子一次的赌注。

那就是幾天後，想邀请雅儿贝德参加舞会。

●

「你这个笨蛋！」

这句怒骂对兴奋的菲利浦泼了一桶冷水，但同时也具有一口气点燃心中怒火的力量。怒火以菲利浦一辈子累积至今的燃料为粮食，熊熊燃烧。

菲利浦不屑地看著眼前满头白髮的男人。

「我让你去不是为了做这种事！你这大笨蛋！」

将王宫自助式宴会上發生的事告诉了父亲的菲利浦叹了口气。

「王室主办的自助式宴会根本不可能寄请帖给我们家，我费尽心力弄到请帖，是为了安排机会让你向伯爵与其他人士致谢──并且把你介绍给他们！」

像王室主办的自助式宴会这种大场合，会有各种派系的人聚集。在这当中隶属於自家派系的家族当家换人，是優先顺序很低的话题。因此这种事不会受到太大重视，都是糊里糊涂就受到认可了。而一旦受到认可，之後就很难再挑毛病。

说穿了就是父亲不信任菲利浦的能力，他是认为照一般方式将菲利浦介绍给派系的人会出问题，所以才这麼做。

菲利浦明白了这一点，拚命压抑住不悦，脸上浮现假笑。

「不不，父亲大人。请您别这麼激动，我是为我们家──」

「──什麼叫我们家！你的所作所为完全是专横跋扈！」

什麼叫做专横跋扈？菲利浦在心中不屑地说。只不过因为尽是些没种付诸行动的胆小鬼，自己才会第一个行动而已啊。

整天顾虑那些无能之辈与胆小鬼，难道是要永远满足於这种卑微地位吗？

「父亲大人！请您稍微想想！虽然远离主要道路，但我们的领地就在魔导国与王都之间。如果魔导国与王国之间爆發战争，不难想像会蒙受战祸，因此我们应该与魔导国加深友好关係。」

「你……你这笨蛋！」

父亲更加涨红了脸怒吼：

「魔导国那些王八蛋可是杀害你哥哥的凶手啊！你竟然要跟那些人同流合污！这岂不是背叛王国的行为吗！」

那又怎样？菲利浦心想。

如果魔导国比较强，就算背叛王国也不会怎样，只要成为魔导国的属臣就行了。弱者跟随更强的强者有什麼不对，谁会责怪他们？

「你究竟在想什麼啊！」

菲利浦对自己父亲的愚蠢感到厌烦。

他觉得要把这麼理所当然的事讲出口很白痴，但看来还是非讲不可。

「很简单啊，父亲大人。这是为了保护我──」

菲利浦把说到一半的「我的」吞回去。不久的将来就会是「我的」了，但目前还不完全属於自己。

「──这是为了保护我们的领地，保护领民啊。魔导国强大无比，比王国更强盛。因此将来对手就算攻打过来也不奇怪，不是吗？所以现在就要建立人脉，为到时候做準备。」

「唔！什麼人脉！这样做周围地区的领主们会怎麼想！」

「他们不敢在这种时局攻打过来的。」

即使在菲利浦的领地内，也有很多人死于那场战争，周边领地内想必也是一样。既然如此，不可能有人还有餘力攻进菲利浦的领地。

「你没有其他想法了吗？」

「啊？」

菲利浦不懂父亲问这什麼意思，因而回问。

「所以我就说你的想法太肤浅了，只会妄想，就以为已经成真了。你这──」

「──还是适可而止吧。」

至今一直在父亲身後静静待著的男人插嘴道。

他是长年侍奉父亲的管家，属於喜怒不形于色的那一型，菲利浦也很讨厌这个男人。等自己做为当家的权力稳固了，他预定将这傢伙也撵出去。

听管家这麼说，父亲调整了一下呼吸。涨红的脸颊渐渐转淡，恢復成原本气色不好的脸色。

「……呼，呼。菲利浦啊，我想问你。除此之外，你不担心与周围贵族为敌，会有其他问题吗？」

「不会啊？」

父亲变得垂头丧气，这种态度让菲利浦既焦躁又不安。

难道自己忘了什麼吗？可是他什麼也想不到。

「卡兹平原的战争当中死了很多年轻人，幾年内想必会出现各种问题。为此，我们必须从现在就跟周围贵族建立互助关係。例如这个领地内生产粮食，那个领地内生产布料等等，大家必须互相帮助。没有人的领地大到能在自己领地内生产所有物资，也没那麼多钱。那麼在这种状况下，你觉得谁会想跟与魔导国谈条件的家族互相协助？」

菲利浦感觉到背後渗出冷汗，父亲说的确实没错。

「你应该也知道吧？我们的领地内并没有生产其他领地不可或缺的物资──也就是特产品。所以就算把我们排除在互助体制之外，对他们也没有影响。」

菲利浦拚命动脑，自己可是很聪明的，随便都能讲到这个白痴父亲无法回嘴。

「──所以才需要魔导国啊，父亲大人。」

父亲要他继续说下去。

「只要跟魔导国建立关係，让他们援助我们就行了。」

「……那你告诉我，如果你是魔导国的人──不，假设你是某个国家的国王，敌国的村庄请你送粮食等物资给他们，你会送吗？」

「当然了，我一定会送。」

「──为什麼？」

「这还用说吗，可以证明我是仁君，对不对？」

「除此之外呢？」

「……没有其他特别理由了。」

父亲微微张开了口，是在佩服自己吗？可是反应好像有点奇怪。不，实际上菲利浦觉得魔导国应该也想要仁君的评价。尤其魔导国统治了前王国领土的耶．兰提尔周边地区。为了这些地区的民众，他们一定很想装好人。

「是吗……你是这麼想的吗。我大概也会提供援助吧，为了当成攻打敌国的藉口之一。我可以说自己是为了解放民不聊生的王国村庄，才对王国發动战争。」

「岂有此理，您疑心太重了。最重要的是，这种藉口怎麼可能被接受？」

「是吗，你不这麼认为吗。」

「更重要的是，如果有父亲说的这种可能，那不是更该与魔导国加深关係？」

「你──」父亲的表情像是愣住了。「你没有身为王国贵族的骄傲吗？」

「当然有，但是就算没有，也总比毁家灭族好吧。」

「那些人拥戴的君王，可是用了令人作呕的魔法，残杀了你哥哥以及众多王国人民的魔王喔？」

「那是战争啊，父亲大人。死在剑下跟死在魔法下又有什麼不同？」

「……你为什麼这麼相信魔导国的国王？」

菲利浦并没有相信他们，的确是有亲近感，但更主要的原因是，他们可以用来创造自己的价值，只不过是提升自己地位的棋子罢了。

（棋子！没错！对我而言王国人民众所畏惧的魔导国之王，也不过是棋子罢了！）

菲利浦彷佛看到自己正在玩无比巨大的──国家等级的棋盘遊戏，而兴奋不已。

（话虽如此，父亲担心得也的确有道理。但也不过就这点程度，一下就被我驳倒了……话虽如此，下次还是跟雅儿贝德阁下说一声吧。）

「我对你已经无话可说了……在自助式宴会上有没有向伯爵大人致谢，感谢他允许你成为当家？」

这件事最让菲利浦无法接受。

凭什麼因为伯爵是派系领袖，自己就得跟个外人低头？

决定下任当家属於领主自治权的範围内，跟伯爵无关。如果伯爵在两个哥哥还在世时推荐身为三男的自己，结果让自己当上领主，菲利浦或许会去道谢；但又不是这麼回事，菲利浦目前的地位全是来自幸运。

换句话说，他没理由跟人低头。

所以菲利浦根本没去跟伯爵低头，但如果这样说，父亲又要激动了。他是考虑到父亲身体不好，才会撒这个谎：

「当然了。」

「是吗？那就好。这样的话还有挽回餘地，有问题时请伯爵提供协助就行了。」

就在菲利浦心想事情终於告一段落时，管家从父亲背後插嘴：

「──还有一个问题，菲利浦大人一开始提到的问题尚未解决，菲利浦大人说邀请了魔导国的使者参加家裡主办的舞会……您打算怎麼办呢？」

「对了，菲利浦！你究竟在想什麼啊！我们家可没有能开舞会的场地。」

地方领主在王都拥有宅邸。

这是为了让领主旅居王都时居住，屋子很小。

当然，是没有平民的家那麼小。虽然一年只会用到幾次，但至少要大到能证明做为贵族的力量，而且能让同行的领民逗留。不过充其量只是大，并没有打造能举办舞会的空间。

不过，这个问题已经解决了。

「不要紧的，在这个家裡的确是不能办，但有人愿意借我场地。」

「哦，难道是伯爵大人？」

看到父亲稍稍转愁为喜地问，菲利浦摇摇头。

「不是的，是我在王都认识的人家，那户人家的女主人说愿意借我场地。实际上我回来之前去见过她，她说没问题。」

「礼金方面呢？」

管家的询问让菲利浦在心中叹气。

他想：第一个问题怎麼会是这个？

「免费。」

「您说免费……有这种事？」

「就是有。」

菲利浦脑中浮现出女主人说过的话：「你看起来很有前途，所以我要对你投资。相对地，将来要加倍还给我喔。」

「我不觉得有这麼好的事……您是不是被骗了？」

菲利浦一阵恼火，但他知道管家备受父亲信赖，目前还不能大声骂他。

「我是欠了人家人情，但同时也想好了偿还的办法，这方面没问题。」

「……那麼先不论会场，请帖呢，要请伯爵大人代发吗？」

父亲在说什麼啊，菲利浦在心中叹息，就是要主办才能提升菲利浦家的名声啊。都做这麼多事前準备了，幹嘛一定要把风头让给别人？

（这就是奴性吗？真可悲……我可不想变成这样。）

「不要紧的，我已经处理好，请借我会场的女主人发请帖。当然，贵宾名单由我负责挑选。」

「……不跟伯爵大人讲一声太失礼了吧，现在还不晚，你应该告诉伯爵大人请他帮忙。真要说起来，你知道你能请哪些贵族家族，而不至於失礼吗？」

「某种程度上知道，而且这次我想请幾位特别嘉宾，名字已经听女主人说过了。」

「你……」父亲眼中浮现怀疑之色：「……你是不是被那个女主人操纵了？」

「父亲大人！您这样讲也太失礼了吧！整件事是由我筹划，由我实行！没错，我是请了人家帮忙。但女主人是听了我的计画，认为有好处──觉得我的计画可行，才会支付相应的代价！而您从刚才就百般刁难，究竟是什麼意思！本来以您的立场，应该全面协助我这个次任当家才对！」

实际上正是如此，女主人说过「只要你能让与我亲近的幾位贵族参加这场舞会，我就帮助你」。是因为对方明确地要求利益，他才会请对方帮忙，才不是什麼被操纵。

女主人才不像握住父亲锁链的伯爵等人，只顾独占利益，什麼都不给他们。

菲利浦很想告诉父亲，他才是被操纵的那一方。

「……抱歉，不过，那个女主人叫什麼名字？」

菲利浦压下怒火，对方奴性还没完全消散，必须以宽大的心原谅他。

「她的名字叫希尔玛．叙格那斯，您有听过吗？」

「不，我没听过。你呢？」

管家也摇摇头。菲利浦因为自己比长年置身於贵族社会的父亲更早认识这个名字，而感到心满意足。

「伯爵大人的事，我也想问问她的意见。不跟她说一声就拜托伯爵大人，说不定会有麻烦。父亲大人，您还有什麼问题吗？」

一脸疲惫的父亲没作声。

虽然心有不满，但菲利浦的计画已经啟动了。再来只要邀请魔导国的使者雅儿贝德小姐莅临，再想想加强自己立场的方法即可。

4

豪华绚烂的会场在菲利浦的视野中铺展开来，不输给他记忆中的王宫会场──不，感觉甚至比那更棒。

他真想随便抓个人来炫耀。没错，这个会场是交给希尔玛準备的。但她曾经这样问过菲利浦：「是要準备普通程度的舞会会场，还是无与伦比的高级会场？若是後者的话，这个人情债会很大。」菲利浦一听，毫不犹豫地选了後者。

换句话说，这个会场是菲利浦欠下一大笔人情债而準备的──也就是他辛苦準备的会场。而他召集而来的众多贵族，就在这会场裡。

太完美了。正因为如此，只有一件事让菲利浦不愉快。

请帖要寄给谁──虽说多少借用了希尔玛一点智慧──是自己决定的，也用自己家族的纹章做了封蜡。最重要的是，到场的所有人都是来见魔导国使者，而这个魔导国使者也是菲利浦找来的。

换句话说，对於自己这个主办人兼大功臣，他们应该低头致谢才对。他们必须感谢菲利浦邀请自己，并赞扬菲利浦勇敢地邀请了谁都不敢上前攀谈的魔导国使者。

然而现实又是如何呢？

来到这裡的所有人第一个致意的对象都不是自己，而是希尔玛，之後才好不容易来向自己致意。而且还是希尔玛提到了菲利浦的名字，才终於知道要致意。要是希尔玛没提到他的名字，真不知道会变成怎样。

由於菲利浦欠了希尔玛一大人情，他必须忍耐希尔玛比自己引人注目，但贵族们就只是让他很不愉快。既然是贵族，照常理来想，应该知道第一个该跟谁致意。

（所以你们才会是废物。啧！也许接受希尔玛的提议是做错了。）

这次叫来的贵族们，是借用了希尔玛的智慧挑选的名单。

挑选出来的，都是与魔导国交战後成为新一代当家，或是不久即将成为当家的人，也就是说立场上跟菲利浦是一样的。

之所以接受了希尔玛的提议，是因为菲利浦以为这种人大多能跟自己感同身受。他认为当家维持不变的家族，很有可能像他的父亲一样对魔导国怀有恶感。

然而──

（全都是些无能之辈吗？）

就在他眼前，刚刚抵达的客人又第一个跑去向希尔玛致意。

菲利浦觉得自己搞砸了。

出不了头的蠢蛋终究只是蠢蛋，所以才会弄错第一个该致意的对象。应该说除此之外，他想不到其他可能性。

（……不，不过，就是这样才好不是吗？都是蠢蛋我才能掌握主导权吧，如果对方是个比我更聪明的贵族，我就当不了新派系的领导人了。况且很遗憾，我家还不是有力量的家族。）

这也是个机会，没有第一个向自己致意是这些人的失态，菲利浦可以当作卖他们一个人情，将来有什麼状况时再让他们还就好。

菲利浦正在打如意算盘时，希尔玛来到他的眼前。

真是个皮包骨的女人。

她病态地削瘦，看起来像生了重病。要是再长点肉应该会是个美女，但那已经是过去的事了。

「菲利浦大人，招待的宾客差不多都到了哟。」

「是吗。」

也就是说所有人都把自己当第二。

面对刺激著自卑感的事实，菲利浦以为自己巧妙隐藏了情绪，但似乎被希尔玛看穿了。

她轻声一笑。

「您似乎很不满意呢。」

「不，没那种事。」

菲利浦微微一笑，他好歹也是个贵族，自认为还满能把话藏在肚子裡。

「请您别这样瞒我，我与菲利浦大人合作，是要从您身上尝到甜头，我们之间不该有秘密。」

这句话带有阿谀谄媚的气息。

就是这个。

菲利浦心裡一阵感动。

这才是贵族与平民之间的正确模样。

他实际感受到，自己现在正坐在长久以来向往的立场，感觉至今的不悦渐渐消失得无影无踪。

「您怎麼了吗，菲利浦大人？」

「没有……这个嘛，我并没有不高兴，只是有点不安。」

「是什麼样的不安呢，有缺了什麼吗？如果是这样的话，我可以在使者阁下莅临之前準备好，如何？」

「不是这样的。」菲利浦趾高气昂地乾咳一声，并回答她的问题：「来到这裡的人，我觉得好像都不怎麼优秀。我担心就算召集这些人成立派系，或许也赢不过别派。」

「原来是这麼回事呀。」

希尔玛脸上浮现笑容。

虽然因为骨瘦如柴而丝毫感觉不到肉慾，但仍然有种蛊惑的魅力，让菲利浦喉咙差点發出咕嘟一声。

「正因为如此，只要由菲利浦大人来领导不就行了？请菲利浦大人想想您的领地，住在那裡的平民们都是些智者吗？」

「不──」

「正因为如此，才需要由智者来当领导人，不是吗？」

「对，没错，你说得对。」

「我相信如果是菲利浦大人，一定能巧妙操纵派系的，我也会尽我所能协助您。」

「为了尝到甜头，对吧。」

「这是当然，我是确定能得到利益，才会帮助您的。」

希尔玛微微一笑。

菲利浦心中的怒火已完全消失。

希尔玛说得很对。

菲利浦感谢自己的幸运，让自己有机会认识希尔玛这名女性。

交际广又有财力，还有王都内的门路等等，不只拥有菲利浦所没有的许多优势，而且清楚说出像这样跟自己友好往来有好处可拿，这样该付什麼报酬就很简单，能够放一百二十个心加以利用。

「只要你帮助我，我会让你比任何女人都更富裕。」

希尔玛似乎稍为睁大了眼睛，然後满足地笑著：

「那真是太高兴了，我早就想做一条贵族女士佩带的那种大颗宝石项炼了。请您多加油喔，菲利浦大人。」

「嗯，包在我身上……那麼我只有一个问题想问合作者阁下，可以吗？」

「是，请说。」

「……你为什麼这麼瘦，身体哪裡不好吗？」

菲利浦之後还得继续让她帮忙，不然就伤脑筋了。如果是连神官都治不好的病，菲利浦得尽快找到代替她的人选，或是让她介绍她的继承人，不然就麻烦了。

「并不是身体哪裡出问题，只是……」

「听说大户人家的千金小姐会为了瘦身而做所谓的减肥，你也是吗？」

希尔玛脸上浮现笑靥，那是菲利浦第一次看到的，难以言喻地，令人感到强烈不安的笑容。

「不是的，其实我不能吃固体食物。所以我只能摄取饮料，但因为摄取不了太多的量，以及一些其他原因……我如果生病了会请人使用治疗魔法，所以这方面请您别担心。」

很快地，她给人的感觉又恢復了原状。

「直到从菲利浦大人身上尝到够多甜头之前，我绝对不会死的。」

「呃，喔，这样啊，那就好。可是……你怎麼会变得不能吃固体食物呢？」

他只是随口问问，但造成的结果却非常大，情感似乎从希尔玛的表情上脱落。

比刚才更大的变化让菲利浦一阵焦急。

「呃，你──您怎麼了？」

「啊，噢，失礼了，只是不小心想起了一些事。」

希尔玛边说边按住嘴巴，脸色很糟。

「啊──真是抱歉，让你想起痛苦的回忆。」

究竟要经歷过什麼事，才会怀抱著不能吃固体食物的心理创伤？看她现在似乎拥有不小门路，过著奢侈的生活，但以前是否有过一段粝食粗餐的时期？菲利浦很想问，但恐怕不该问。

「菲利浦大人，我想差不多该请使者大人来场了。只要菲利浦大人护送她入场，大家一定都会对您另眼相看。比起千言万语，这样应该更能明确传达谁才是这场舞会的主办者──最有力量的人。」

「哦！差点忘了。」

她在王宫的自助式宴会是一个人现身，菲利浦本来以为那很正常，看来似乎并非如此。因为不知道太丢脸了，所以他假装自己太迷糊，现在才终於注意到。

「大家一定会很惊讶，许多没来跟菲利浦大人致意的人，必定会焦躁不安。」

菲利浦心中浮现出嗜虐的喜悦，聚集於此的贵族们当中，有人比自己地位更高，也有人领地比自己更大。这些人会用什麼表情排在自己面前？而且是过去吃家裡闲饭的自己面前──

「说得对，也不好让她久等，我这就去请她。」

「那麼我让人带您走到半路。」

菲利浦让希尔玛叫来的服务生带领著，前往魔导国使者雅儿贝德等待的房间。

他敲敲门，然後才开门。

房裡有位美貌过人的女子。

她身穿不同於王宫那件的漆黑礼服，露出的肩膀散发著白石光辉。脖子戴著大颗宝石排列成的项炼，但毫不庸俗，只是为她的美貌起了小小锦上添花之效。

（真美……）

菲利浦不禁脸红。

「──那麼我们走吧？」

「是，容我护送您前往会场。」

他执起戴著黑蕾丝手套的玉手，让雅儿贝德站起来。

站到她身边，就闻到一股怡人的芳香。这是什麼香水？香味令人心情快活。菲利浦产生一种想抽动鼻子多闻幾下的衝动，但实在不能那麼做。

两人并肩走向会场，但始终没有对话，让气氛有点沉重。菲利浦拚命思索有没有什麼好话题，来到宴会会场的门扉附近，才好不容易想到话题。

「──会场裡聚集了众多贵族，大家都很想见到雅儿贝德大人。」

这话题虽然唐突，但她立刻配合著回答：

「是这样呀，感谢菲利浦大人的帮助。」

雅儿贝德对他露出亲密的笑容。

菲利浦的心臟重重跳了一下。

照理来说应该不可能，但她会不会是对自己有点好感？

自己再过不久就会是大派系的首脑。魔导国虽然拥有压倒群雄的武力，但毕竟是只有一座都市的国家。

这麼想来，自己或许条件相当不错？

而且正好还没娶妻。

「对了，雅儿贝德大人是否已有夫婿？」

雅儿贝德表情一愣，睁大眼睛。菲利浦看过她的温柔笑靥好幾次，但这种表情还是第一次看到。

菲利浦知道自己问了个怪问题，觉得有点难为情。

「真是个独特的问题，菲利浦大人。非常遗憾，我没有良人，还是个寂寞单身之人。」

「这样啊，我以为像雅儿贝德大人这样美丽的女士，不用自己开口，多的是男士争相求婚呢。」

「呵呵──很不可思议地，并没有这样的事。话虽如此，要是有也很伤脑筋，所以我也觉得庆幸就是了。」

「原来是这样啊。」

菲利浦来到门前，将手绕上雅儿贝德的肩膀，将她搂向自己。

他听见奇怪的「叽嗤」一声，为了找寻声音来源而转头。

「……怎麼了吗？」

面带笑容的雅儿贝德一问，这点小疑问就从脑海中消失了。

「没有，没什麼，那麼容我领著您进会场。」

●

他们的眼中都看到了什麼？

对於这些穿金戴银的贵族们是怎麼看这个舞台的，希尔玛抱有些许兴趣。

一流的料理、一流的服务、一流的用品、一流的音乐，以及三流以下的垃圾贵族们。

聚集於此的很多人都是米虫或三男以下备用品的备用品，因为各种原因而出不了头，心中怀著不满的人。

看他们的脸就知道了。

很多人一脸获得解脱的开朗神情，也有很多人被慾望之火焚身。

对於这些人来说，这个会场正能满足自己的虚荣心。

然而，这裡本来应该是饲料场才对。

目前，王国的贵族社会正處於混乱当中。

即使经过了幾个月，与魔导国之战造成的伤痕仍然很大，未曾治癒。幾个派系解散，新的派系诞生。原本居於高位的贵族家族，被下面的贵族取代。

目前王国的混乱，对於那些因为各种理由而无法加入派系的人来说是个大好机会。不，应该说这是最後机会。因为一旦派系再度整合起来，他们又会被赶到角落。所以这场聚会对他们来说，应该是个巨大的饲料场才对。

饥肠辘辘的鱼群展开行动，欲将小鱼吞进腹中。

对於这个状况，小鱼是否会无法察觉眼前对手的目的，而被一口吃掉？还是会察觉危机而巧妙游走？抑或是──有没有哪个贵族能反过来咬住对手，贪婪吞食？

希尔玛望著会场的动向幾十分钟，结论是她可以断定，这裡没有一个称得上一流，会让她想全力拉拢的贵族。

不过，她并不失望。要是有哪个一流贵族若无其事地出现在这种危险的会场，那极有可能是间谍。

虽说寄送请帖时已经过滤了，但希尔玛也不觉得能百分之百排除乾净，一定已经有哪个派系的人潜入了会场。

那样也很有意思，她想。

因为这样能让交出的报告书更有深度，提升自己的价值，对她来说不是件坏事。

（好了，差不多是时候了吧？）

舞会开始已过了一个半小时，指定的时间到了。

希尔玛真正的工作现在才要开始。

──好可怕。

方才的傲慢难以置信地逐渐消失。

用可怕这种温和词语不足以形容的恐惧从胃裡往上涌升。一想到要是惹大人不高兴，那个地狱或许又会等著自己，她就想卯足全力逃离这裡。当然她要是这麼做，就连那个都有如天堂的残忍刑罚想必将会等著自己。

身为八指成员之一，她一直以来下过无数次杀人指示，有时还会命令手下要让对方尝尽痛苦再死。然而不管是哪次命令，比起那些怪物对待她的方式，简直都是慈悲心肠了。

「──希尔玛。」

背後有人出声叫她，令她肩膀差点一震。

回头一看，是会场内最愚蠢的男人。

「嗯，怎麼了？」

「没有，菲利浦大人，没什麼。」

希尔玛将真心话藏在笑容底下，她气自己竟然被这种废物吓到。

「雅儿贝德大人说想休息个十分钟，正在找你。」

「我看大人一直在和各位贵族谈话，都没休息，会累也是当然的呢。我明白了，那就由我带大人前往休息室。」

「是吗？那我也跟去吧。」

这人在说什麼啊，希尔玛实在受不了他。不，还是说他察觉到什麼了？

希尔玛怀著戒心，但继续演戏：

「我想最好不要。」

「为什麼？我刚才一直待在雅儿贝德大人身边，跟她一起去也不奇怪吧？」

希尔玛确定这个男的是真的不懂。

换个说法就是白痴中的白痴，毫无身为贵族的礼仪与知识，是个无能之辈。

「一般来说，丈夫以外的男性陪伴女性去其他地方休息，会让各位人士闲言闲语的。」

「喔──但我只是带她过去，马上就回来啊。」

「还是会让人讲闲话的。我明白您身为主人会担心，但我也是提供会场之人，会将雅儿贝德大人平安带到休息室的。」

「嗯……」

看他好像还想说什麼，希尔玛等他继续说下去。

其实希尔玛很想叫他快说，但这个白痴好歹也是主办人，态度不能太失礼。

「你觉得我该怎麼做，才能跟她成婚？」

「啊？」希尔玛听到他的下一句话，一时忘了演戏。「咦，您说什麼？」

「我是说有什麼办法让我跟雅儿贝德大人结婚。」

「这傢伙是认真的吗！」希尔玛死命压抑住想这样大叫的心情，真没想到这人可以白痴到这个地步。依照希尔玛收集的情报，对方可是那魔导王的左右手──地位如同宰相。对这样的贵人，邻国的低级贵族实在不该说出这种话。

问希尔玛怎样才能跟拉娜公主结婚，她都不至於这麼惊愕。

「呃，我是觉得我有办法召集这麼多贵族，再怎麼想也不比她差，你觉得呢？」

希尔玛不知不觉间，用力按住了自己的喉咙。

即使知道那个不会沿著喉咙滑下，深铭肺腑的心理创伤带来的不安与恐惧，仍然让她做出这种动作。

不，用心理创伤还不足以解释。

女人看来毫无魅力的一个男人口出如此戏言，要是那位大人听到，不知会作何感想。如果她的矛头朝向菲利浦的话还无所谓，但要是朝向自己，那个黑色地狱也许会等著自己。

「我……我想这实在办不到。听说那位大人在魔导国相当于宰相地位，就算与王国的公爵等同视之也不为过。」

「可是，魔导国不是只有一座都市的国家吗？」

「呃，不，不是这种问题……」

轻视魔导国的發言让希尔玛浑身起满鸡皮疙瘩。

的确即使将卡兹平原等地算进去，魔导国的领土也不算大，但他们的武力可是无人能及。就算在贸易或外交方面如何努力，国与国的关係终究还是以蛮力强弱决定。领土再怎麼大，一旦打输就只能拱手让人。

这个白痴连这都不懂，要怎麼讲才能让他接受？

希尔玛左思右想，但想不出答案。因为常识与白痴是两极的存在。

所以她只能拿出结论说服他：

「没办法，她绝不可能跟菲利浦大人结婚。」

「……我觉得我们之间气氛还不错啊，像我跟她一起走进会场时，看起来不是满亲密的？」

这傢伙当时站在那裡，是在想这种事？希尔玛大吃一惊。

（不是在用态度强调「魔导国是自己的後盾」，拉拢客人们加入自己的派系？这傢伙真是白痴到极点了……拜托饶了我吧，不要刺激那位大人啊。）

希尔玛感觉一股苦味从胃裡涌了上来。

同时她也产生一种心情，想让这傢伙也尝尝流进胃裡的那种感觉。

「……话似乎讲得有点久了，我会陪雅儿贝德大人去休息，请菲利浦大人留下来，以主办人的身分让大家尽兴吧。」

「……既然如此也没办法了，雅儿贝德大人就拜托你了。」

不用你说我也知道。希尔玛没说出口，轻轻低头。然後她不想再听更多蠢话，就一直线往雅儿贝德身边走去。

雅儿贝德正在与一名贵族说话，平常希尔玛会察言观色，找个恰当的时机出声，但她刚才被白痴搞得很累，於是马上就向雅儿贝德说道：

「恕我冒昧，雅儿贝德大人，我想您差不多该休息一下了。」

「也是……恕我失礼，我想稍微休息一下。」

她让雅儿贝德跟在後面，走出会场。

「呼……啊，真恶心。」

听到背後传来的声音，希尔玛转头看了看。她是在想如果雅儿贝德真的不舒服，自己该怎麼办。

一看，她正在用手帕擦自己的肩膀。

雅儿贝德与希尔玛四目相接。

「我被恶心的男人摸了啦。这世界上能带著情欲碰我的，明明只有一位大人……臭王八蛋，那个没智商的臭东西。」

希尔玛听见了咬牙切齿的叽叽声。总是维持著温柔笑靥的她表示出明显的不悦，看来那男人真的令她反胃至极。

希尔玛犹豫了，跟她讲话不会有问题吗，还是说这是在为惩罚做準备？

「……怎麼了？我们说说话吧。」

「好……好的……」希尔玛内心吓得魂飞魄散，开口道：「我能体会雅儿贝德大人的心情。」

「哎呀，既然如此……能不能把那个捨弃了，现在再换另一个人？」

「只要雅儿贝德大人有意，我立刻另外準备一个人偶。」

雅儿贝德启唇，又合起来，重複了幾次这个动作。

看来这项提议真的很吸引她，令她不禁犹豫。

无论选择哪一个，都只有地狱等著愚蠢的菲利浦，但希尔玛只觉得他自作自受。

「呼……别放在心上，我只是抱怨两句。他的愚昧程度在王宫的自助式宴会上，已经给了许多贵族深刻印象。从这层意义来说，换人好像也有点可惜……如果那人是想到这一切才这样行动，那还满有意思的，但我看不可能。」

希尔玛想起刚才的对话，想起那个狂人胡说什麼要跟雅儿贝德结婚。

要是把那件事告诉雅儿贝德，不知道会有什麼後果。

希尔玛怕得要命，绝对不敢告诉雅儿贝德。搞不好自己还会遭到池鱼之殃。

「明明一事无成，却自命不凡，真是个无可救药的无能之辈。」

「就是啊，再过一阵子，我们就狠狠把他打落地面吧。竟然敢用脏手乱摸安兹大人的女人，我可得好好惩罚他才行。」

後来两人都没开口，也没见到任何人，希尔玛带著雅儿贝德来到一个房间前面。

来到门前，希尔玛恨不得能安心地一屁股跌坐在地。由自己一个人面对她──面对连亚达巴沃都心悦诚服的魔王的近臣，不知道磨损了她多少精神。但对方不可能准许她瘫坐地面。

希尔玛振作起全副心力，暗自决定等这件事结束了，要睡个一整天。

「就是这裡。」

希尔玛打开房门後，坐在椅子上的男人们一齐站了起来，每个男人都跟希尔玛一样削瘦。

这些人是她的同僚，是八指各部门的五名首脑加上议长，总共六人。

换句话说，这些是她在这世界上最能信任的自己人。过去他们曾经互相争鬥，但如今已经没人这麼想了。既然大家都知道了亚达巴沃与魔导国的关係，就是坐在同一条船上。他们必须一起做牛做马，直到这个国家被吞没，获得解放之时。

这些甚至让希尔玛感到亲密的同伴们，一看到恐惧的化身（雅儿贝德）到来，全都压低了头行礼，隐藏不住的惧意显现在颤抖的肩膀上。

雅儿贝德让希尔玛关上门，在放在房间上座，最昂贵的椅子上坐下。男人们与希尔玛都不敢坐，维持立正不动的姿势，等著她發号施令。

「好了，我要给你们命令。首先，我要你们将各种物资运进魔导国。」

「遵命，我很乐意献给大人。」

走私长毫不迟疑地回答，不可能有所迟疑。当他们被叫到这裡时，对於所有一切命令都只能回答「遵命」。

身为走私长的他在亚达巴沃骚乱中被夺走许多物资，失去了对商人公会等组织的影响力，但地位仍然无可撼动。这是因为他在与参加魔导国战争的贵族做生意时，彻底坚持即时付现。或许该说如今答应事後付款的商人们苦不堪言，才会让他的权力再度浮上表面。

「不是这样，我要你以适当价格卖给我们。然後用赚来的钱买进粮食，为王国即将面临的粮食危机做準备。王国军没能运走的大量粮食──不，就进行粮食的期货交易吧。安兹大人已经为这件事开始推动粮食的大量生产了。」

如今王国失去了大量劳动力，她所说的情形将来必然發生。

「遵命，我立刻让商人们前往。」

「特别需要的是这些东西，让第一批人好好带著。」

走私长恭敬地接下扔在桌上的纸。

「是！」

「还有，关於魔法道具的情报怎麼样了？」

另一个人像被电到般动起来。

「非常抱歉！」

他弯下腰，把额头狠狠撞在桌上，發出的声音大得惊人。

「我已经派手下潜入魔法师工会，正在详细调查！请再给我一点时间──不，如果有需要，我可以立刻提供中间报告！」

「那没关係，你尽快行动。再来嘛，对了，你们的新同僚人选决定了吗？决定的话，我得把他带回去进行洗礼才行。」

所谓的同僚，指的是填补空缺的八指新部门长。

希尔玛想起洗礼指的是何种行为，忍住反胃感。同伴们虽然拚命压抑表情，但也都是同一副表情。

那种恶魔的洗礼能令人心志颓丧，完全失去敌对念头。如果有人叫在场的所有人再去受一次那种洗礼，他们绝对会像小孩一样大哭大叫。

「很遗憾，还没决定。」

议长开口了。

这是实话，同时也是谎话。

这是因为再找个新人来当头子也没意义。空出的位子是警备长与奴隷买卖长，以後者来说，现在幾乎没人在做奴隷生意，就算新安排个负责人也没太大好处；前者更是令人怀疑有没有存在意义。再说──

「向大人借用的各位人士非常优秀，或许可以由他们来担任部门长。」

魔导国向他们提供了不死者，而且每个都具有难以置信的力量。

听到六臂已死，出现了一些以工作者出身为主的冒失鬼，於是他们派不死者过去；结果才一只就杀掉了将近四十人，没让任何一个人逃走。

另外还有一个好笑的理由，就是在场所有人都不愿有人跟自己遭到同样的命运。指示杀人从不为所动的黑社会支配者们竟然袒护他人，不希望有人跟自己一样尝受那种绝望滋味。

「……我知道了，只要不会影响组织运作，这样也行。那麼，你们有没有什麼想拜托我们的？」

「恕我斗胆，骷髅们在我向大人借用权利的矿山做出了相当好的成果。因此，希望能让我再借用他们一阵子。」

「嗯，当然好。只要支付适当金额，就可以继续租借。」

「多谢大人。」

开口说话的男人用手帕擦拭额上汗水，手帕湿到都变色了。

魔导国的可怕之处，在於恩威并济。

他们不是恃强欺弱，抢走弱者手中的一切，而是有如精明商人般谈生意，而且遵守规定。事实上只要不表示出叛心，他们甚至会给人一种受到强大存在保护的安心感。当然像这样站在自己面前时，还是会害怕得想逃走就是。

「好了，我之所以直接来见你们，理由不用说也知道。我想应该跟你们说过了，为了将来让魔导国吞并王国，你们必须全面配合。为此，你们在一般社会也得稳定扎根。」

「遵命！」

所有人急忙低头行礼。

他们不可能反对魔导国吞并王国，既然那些怪物如此断言，那就只是时间早晚问题，是必然的结果。

起初也有人提过请苍蔷薇、朱红露滴或漆黑前来救援，但当他们听说魔导王的力量无人能及，连亚达巴沃都是他的部下，就知道已经没有任何希望了。他们只能俯首称臣，等待最後的时刻到来。

「对了对了──」

希尔玛与其他成员都肩膀一震。

「我忘了讲一件事，希望用你们的情报网络帮我找个魔法道具。然後定期把结果写在羊皮纸上，寄给魔导国的雅儿贝德。只不过关於它的外观等等，我没有任何情报就是了。」

「……那是个什麼样的道具呢？」

「是能够控制对方精神的道具。」

「控制精神……是不是魅惑等魔法的短杖呢？」

「不，我想是更强力的道具。我希望你们打听的，是一般市场不会流通的传说级道具。不管任何芝麻小事都要告诉我，知道吗？」

精神控制具有非常可怕的效果。

他们心想雅儿贝德会提高警戒也是理所当然，立刻表示了解。

●

「公……公主殿下。」

女仆态度慌张地进了房间。

连门都不敲，态度实在不可取，但这也证明了她的确相当慌张。

拉娜立刻猜出發生了什麼事，但是在女仆面前，拉娜是个天真无邪的小公主。她用符合这种角色的表情与傻气态度问道：

「怎麼了吗？」

女仆的外眼角动了一下。

大概是心中涌起了愤怒情绪吧，很可能是因为自己这麼慌乱，这个小公主却一愣一愣的关係。

拉娜悠哉地把茶杯放回小碟子上。

以轻敲的声响为契机，女仆动了起来。

「那……那……那个──」

「好的，不要紧的。冷静下来，做个深呼吸。」

女仆听从拉娜所言，重複做幾个深呼吸，让喘吁吁的呼吸恢復平顺。看到她稍微恢復平静，拉娜才问道：「怎麼了，又有恶魔出现了吗？」

「不……不是的。魔导国的使者阁下表示想晋见拉娜大人！」

「对方是女性吗？」

「是的，是位非常美丽的女性。」

魔导国就只有这麼一位使者，拉娜这样问应该很奇怪。她本来是故意说错，想让女仆觉得「这人在说什麼啊」，但女仆心都乱了，回答得很认真。

拉娜心想：「好吧，无所谓。」这种小细节累积起来，会带来可供利用的评价，一切都是布局。

近旁待命的克莱姆，铠甲發出了摩擦声。

也许他是偏了偏头吧。

想到可爱小狗的天真行动，拉娜胸中涌起慈爱之情。

拉娜猜想，克莱姆一定是不明白使者为何要来找拉娜。拉娜与使者已经互相致过意，克莱姆也看到了。所以他应该不觉得使者特地找只是个花瓶的第三公主说话，能为魔导国带来什麼好处吧。

拉娜在心中温柔微笑。

愈笨的孩子愈可爱的确是事实。不对，严格来说应该是情人眼裡出西施吧。大概两个都对。

因为如果是克莱姆以外的人这样做，拉娜心中涌现的就不是这种情感了。

拉娜巴不得能一直看著克莱姆闪闪发亮的眼神，但现在必须忍耐，直到她用甜蜜的糖衣将克莱姆包裹起来那瞬间。

「雅儿贝德大人究竟为什麼要来见我呢？」

重要的是要偏偏头，经过幾次实验，拉娜知道这样会让心急的人产生反感。

事实上，女仆眼中的确摇曳著微微火光。

是怒火。同时，克莱姆的铠甲再度發出细微声响。

大概是察觉到女仆的情绪，心裡有点意见吧。但声音马上就停了，一定是又恢復了不动的姿势。

真可爱。

就像犹豫著该不该上前保护主人的小狗。

克莱姆大概是判断如果拉娜没發现，就不要行动比较好吧。他一定是想：女仆是家世显赫的贵族千金，要是出身不明的自己说些什麼，女仆搞不好会跟父母告状，结果给拉娜造成困扰。

信赖拉娜的他，想必心中正在流泪：要是我的家世好一点，哪裡会让女仆这麼放肆。

拉娜强压住想看站在背後的克莱姆的慾望，因为碍事的女仆开口了：

「这我也不知道，对方只说想见您。」

「这样啊……雅儿贝德大人也是位女性，或许有些女人家的事想聊聊吧……会不会是化妆的话题呢？」

她天真无邪地──应该说像个白痴似的问道。

「这我无从知道，那麼我可以带使者阁下过来吗？」

「当然了！」

拉娜装出开心的模样回答後，转向克莱姆那边：

「呃，克莱姆。不好意思，我们两个女人有话要讲，可以请你离开房间吗？」

「遵命。」

虽然有点遗憾，但没办法。克莱姆不用知道任何困难的事，只要用那双清澈的眼睛看著自己就行了。

雅儿贝德走进房间时，裡面只有一人。

雅儿贝德来到王都有四个目的。

第一是让人运送物资，第二是制造引發战争的契机，第三是为个人目的布局，而第四是与这个房间的主人做交易。

不，交易这个说法不太对，应该称之为赏赐。

雅儿贝德没徵求房间主人的同意就横越房间，坐在放著的椅子上。

然後对屈膝跪在自己跟前，低垂著头的少女开口道：

「抬起头来。」

「──是。」

名为拉娜的少女抬起了头。

「你做得非常好。」

「谢谢雅儿贝德大人。」

「哎呀──」

她与至今见面时截然不同的反应，大大刺激了雅儿贝德的兴趣。

这才是迪米乌哥斯谈到的拉娜。

即使背叛了自己的家人、血统与子民，表情仍没有後悔之色。这个存在似人非人，应该称之为精神异类。她的头脑应该能理解善与恶，但纯粹只是理解，属於不受善恶观念束缚，能平静自若地达到自己目的的类型。

「……安兹大人赞赏你的功劳，命我带来奖赏。」

雅儿贝德从空间当中取出她的主人交给她保管的道具。

小盒子施加了好幾层封印，除非满足特定条件，否则盒子绝对打不开。

「这就是……」

少女满怀感激地接过盒子，雅儿贝德用研究者看白老鼠的冰冷目光看著她。

她正是一只实验白老鼠，所以双方的利害关係才会一致。

「谢谢大人，也请您向安兹．乌尔．恭大人传达我的谢意。」

「我答应你，关於你想要的另一个东西，不用我多说了吧？」

「当然，等我支付了合理的代价时，只要大人能慈悲待我，就是我无上的喜悦了。」

少女微笑著。

那笑容相当可爱。

所以雅儿贝德问她：

「……只要打开那个盒子，你的心愿就会实现，但你能打开它吗？」

雅儿贝德竟然会担心人类，要是让纳萨力克的成员们知道了，他们不知会怎麼想。然而，当她的心愿实现之时，已经安排赐予她与领域守护者同等的地位。多少担心一下将来的部下候补，也不会遭天谴吧。

「是，雅儿贝德大人，我已经在做準备了。」

「是吗？那麼在我们进攻之前，你得做好一切準备。」

「遵命，伟大的贵人。」

雅儿贝德的目光从再度低头的少女转向她的影子。

潜伏在那裡的暗影恶魔滑溜般现身，跟少女一样低头行礼。

雅儿贝德本来在想也许该给点追加兵力，但又把话吞了回去。

如果在魔导国进攻王国之前，这个少女的所作所为曝光了，那也只不过表示没有将她拉拢进纳萨力克的价值。

说起来，这等於是一项测试。

「那麼严肃话题就到此为止吧。」

雅儿贝德的语气变了，拉娜露出不解的表情。

「现在离开还太早了，我们聊聊──来话家常好了。来，坐吧，跟我聊聊你的小狗狗好吗？」

她露出满面的笑容迎向雅儿贝德。

「乐意之至，雅儿贝德大人。还有，如果可以的话，能否也请您跟我说说安兹．乌尔．恭大人的事呢？」
