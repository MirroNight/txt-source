__TOC__

[蜘蛛ですが、なにか？](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E8%9C%98%E8%9B%9B%E3%81%A7%E3%81%99%E3%81%8C%E3%80%81%E3%81%AA%E3%81%AB%E3%81%8B%EF%BC%9F.ts)  
總數：11／11

# Pattern

## 艾兒

### patterns

- `阿艾爾`

## 莉兒

### patterns

- `利艾爾`
- `利艾兒`

## 菲兒

### patterns

- `菲艾爾`

## 莎兒

### patterns

- `薩艾爾`

## 蘇菲亞

### patterns

- `蘇菲雅`
- `蘇菲亞`
- `索菲婭`
- `索菲亞`
- `蘇菲婭`

## 波狄瑪斯

### patterns

- `破提馬斯`
- `波蒂馬斯`
- `波狄瑪斯`
- `破提納斯`
- `ポティマス`

## 海非納斯

### patterns

- `ハァイフェナス`

## 連克山杜

### patterns

- `連古贊德`
- `聯古占托`
- `連克山杜`
- `レングザンド`

## 公會長

### patterns

- `公會老板`


